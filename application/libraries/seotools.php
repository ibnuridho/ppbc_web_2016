<?php

    class SeoTools {
        /* SeoTools () : Digunakan untuk merubah PageTitle, MetaKeywords, dan PageDesc dalam setiap Page */

        public function seo_tools($case, $id = 0) {
            switch ($case) {
                default:
                    $data['PageTitle'] = 'PERSATUAN PENSIUNAN BEA & CUKAI | PPBC.com';

                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA & CUKAI';
                    $data['PageDesc'] = 'PERSATUAN PENSIUNAN BEA & CUKAI';
                    return $data;
                    break;
                case 'archives' :
                    $data['PageTitle'] = 'Arsip Order | PPBC.com';

                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    return $data;
                    break;
                case 'detail':
                    $CI = & get_instance();
                    $CI->load->database();
                    $sql = "SELECT prime_image, name, highlight, meta_keyword, meta_description FROM products WHERE id=" . $id;
                    $query = $CI->db->query($sql);
                    $trip = $query->row();
                    $data['PageTitle'] = $trip->name . " | PPBC.com";
                    $data['PageKeyWords'] = $trip->meta_keyword;
                    $data['PageDesc'] = strip_tags($trip->meta_description);
                    $data['image'] = $trip->prime_image;
                    return $data;
                    break;
                case 'cart':
                    break;
                case 'account':
                    $data['PageTitle'] = "My Account | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'register':
                    $data['PageTitle'] = 'Registrasi Member | PPBC.com';
                    $data['PageKeyWords'] = 'Traveller dan bermanfaat bagi kalian yang suka banget travelling. Package produk Travelling ini akan membantu kamu menyiapkan paket perjalanan anda!';
                    $data['PageDesc'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    return $data;
                    break;
                case 'Login':
                    $data['PageTitle'] = "Login | PPBC";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    return $data;
                    break;
                case 'about_us':
                    $data['PageTitle'] = "About Us | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'contact_us':
                    $data['PageTitle'] = "Contact Us | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'maskapai':
                    $data['PageTitle'] = "Available Maskapai | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'free_mode_detail':
                    $data['PageTitle'] = "Free Mode Detail | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'package_mode_detail':
                    $data['PageTitle'] = "Available Trips | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'how_to_buy':
                    $data['PageTitle'] = "Cara Memesan | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'custome_request':
                    $data['PageTitle'] = "Request new Trip | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'faq':
                    $data['PageTitle'] = "Frequently Asked Questions | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
                case 'termconditions':
                    $data['PageTitle'] = "Term and Conditions | PPBC.com";
                    $data['PageKeyWords'] = 'PERSATUAN PENSIUNAN BEA CUKAI';
                    $data['PageDesc'] = "PERSATUAN PENSIUNAN BEA CUKAI";
                    return $data;
                    break;
              }
        }

    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
    ?>
