<?php

class MasterAccountBankModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('master_account_bank', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('master_account_bank', $data);
        return $id;
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('master_account_bank');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('master_account_bank');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('master_account_bank');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}