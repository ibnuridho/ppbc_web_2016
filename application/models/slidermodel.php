<?php

class SliderModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('slider', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('slider', $data);
        return $id;
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('slider');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('slider');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('slider');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}