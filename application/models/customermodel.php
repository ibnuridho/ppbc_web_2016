<?php

class CustomerModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('customers', $data);
        return true;
    }

    function Add($data) {
        $this->db->insert('customers', $data);
        return $this->db->insert_id();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('customers');
        return true;
    }

    function GetAllCustomer() {
        $this->db->select('*');
        $this->db->from('customers');
        return $this->db->get();
    }

    function CheckUserName($username) {
        $this->db->select('username');
        $this->db->from('customers');
        $this->db->where('username', $username);
        $result = $this->db->get();
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function CheckEmail($email) {
        $this->db->select('email');
        $this->db->from('customers');
        $this->db->where('email', $email);
        $result = $this->db->get();
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

//    check customers by email called when login procsess
    function CheckCustomerByEmail($email, $password) {
        $this->db->select('*');
        $this->db->where('LOWER(email)', $email);
        $this->db->where('password', $password);
        $this->db->from('customers');
        return $this->db->get();
    }

//    check customers by username called when login procsess
    function CheckCustomerByUsername($username, $password) {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->from('customers');
        return $this->db->get();
    }

    function GetCustomerByID($id) {
        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('id', $id);
        $query =  $this->db->get();
        return $query->row();
    }

    function GetCustomerDetail($id) {
        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function AddContact($data) {
        $this->db->insert('message_customer', $data);
        return $this->db->insert_id();
    }

}