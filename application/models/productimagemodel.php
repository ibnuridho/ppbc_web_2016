<?php

class ProductImageModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('product_images', $data);
        return $this->db->insert_id();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('product_images');
        return true;
    }

    //delete image berdasarkan product id, jika product dihapus fungsi ini akan dipanggil
    function DeleteByProductID($product_id) {
        $this->db->where('product_id', $product_id);
        $this->db->delete('product_images');
        return true;
    }

    //mengambil nama mimage berdasarkan id
    function GetImageNameByID($id) {
        $this->db->select('image');
        $this->db->from('product_images');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result->image;
    }
    
    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('product_images', $data);
        return true;
    }

        //mengambil nama mimage berdasarkan id
    function GetImageByID($id) {
        $this->db->select('*');
        $this->db->from('product_images');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
}