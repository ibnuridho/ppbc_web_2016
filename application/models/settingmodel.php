<?php

class SettingModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('settings', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('settings', $data);
        return $id;
    }

    function getBannerMaskapai() {
        $this->db->select('*');
        $this->db->from('settings');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('settings');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('settings');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('settings');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}