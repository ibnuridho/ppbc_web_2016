<?php

class PageModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('pages', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('pages', $data);
        return $id;
    }

    function GetLatestpages($limit) {
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->limit($limit);
        return $this->db->get();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('pages');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('pages');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}