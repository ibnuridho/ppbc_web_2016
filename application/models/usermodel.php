<?php

class UserModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        return true;
    }

    function Add($data) {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('users');
        return true;
    }

    function GetAllUser() {
        $this->db->select('*');
        $this->db->from('users');
        return $this->db->get();
    }

    function CheckUserName($username) {
        $this->db->select('username');
        $this->db->from('users');
        $this->db->where('username', $username);
        $result = $this->db->get();
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function CheckEmail($email) {
        $this->db->select('email');
        $this->db->from('users');
        $this->db->where('email', $email);
        $result = $this->db->get();
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

//    check users by email called when login procsess
    function CheckUserByEmail($email, $password) {
        $this->db->select('*');
        $this->db->where('LOWER(email)', $email);
        $this->db->where('password', $password);
        $this->db->from('users');
        return $this->db->get();
    }

//    check users by username called when login procsess
    function CheckUserByUsername($username, $password) {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->from('users');
        return $this->db->get();
    }

    function GetUserByID($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query =  $this->db->get();
        return $query;
    }

    function GetUserDetail($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

}