<?php

class OrderModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('order', $data);
        return $this->db->insert_id();
    }

    function AddRequestTrip($data) {
        $this->db->insert('trip_request', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('order', $data);
        return $id;
    }

    function getNewOrder() {
        $this->db->select('*');
        $this->db->from('order');
		$this->db->where('is_read', 0);
        return $this->db->get();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('order');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('order');
        $this->db->orderBy('modified_at','DESC');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('order');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function GetDatarequestByID($id) {
        $this->db->select('*');
        $this->db->from('trip_request');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getAllRequest() {
        $this->db->select('*');
        $this->db->from('trip_request');
        // $this->db->orderBy('modified_at','DESC');
        return $this->db->get();
    }
}