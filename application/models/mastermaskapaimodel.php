<?php

class MasterMaskapaiModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('master_maskapai', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('master_maskapai', $data);
        return $id;
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('master_maskapai');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('master_maskapai');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('master_maskapai');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}