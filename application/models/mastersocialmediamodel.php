<?php

class MasterSocialMediaModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('master_social_media', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('master_social_media', $data);
        return $id;
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('master_social_media');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('master_social_media');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('master_social_media');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}