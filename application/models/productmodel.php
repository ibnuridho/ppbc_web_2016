<?php

class ProductModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('products', $data);
        return $id;
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('products');
        return true;
    }

    /* checking product name by slug */
    function CheckExistingProductBySlug($slug) {
        $this->db->select('slug');
        $this->db->from('products');
        $this->db->where('slug', $slug);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function GetLatestTrips($limit) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->limit($limit);
        $this->db->order_by("created_at", "DESC");
        return $this->db->get();
    }

    function GetTripIDBySlug($slug) {
        $this->db->select('id');
        $this->db->from('products');
        $this->db->where('slug', $slug);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row();
            return $result->id;
        } else {
            return false;
        }
    }


    function GetAllProduct() {
        $this->db->select('*');
        $this->db->from('products');
        return $this->db->get();
    }

    function GetProductByID($id) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function GetImagesTripByID($id) {
        $this->db->select('*');
        $this->db->from('product_images');
        $this->db->where('product_id', $id);
        $this->db->limit('4');
        $this->db->order_by('created_at','DESC');
        return $this->db->get();
    }
    
    function GetImagesProductByID($id) {
        $this->db->select('*');
        $this->db->from('product_images');
        $this->db->where('product_id', $id);
        return $this->db->get();
    }

    function GetFilesProductByID($id) {
        $this->db->select('*');
        $this->db->from('product_files');
        $this->db->where('product_id', $id);
        return $this->db->get();
    }

    /* get product id by slug */
    function GetProductIDBySlug($slug) {
        $this->db->select('product_id');
        $this->db->from('product');
        $this->db->where('slug', $slug);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row();
            return $result->product_id;
        } else {
            return false;
        }
    }
    
    function UpdateDescriptionImage($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('products', $data);
    }

}