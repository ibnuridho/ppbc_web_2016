<?php

class BannerModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('banner', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('banner', $data);
        return $id;
    }

    function GetLatestBanner($limit) {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->limit($limit);
        return $this->db->get();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('banner');
        return true;
    }

    function GetAllData() {
        $this->db->select('*');
        $this->db->from('banner');
        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function GetAllDataPage($params){
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('position', $params);
        return $this->db->get();
    }
}