<?php

class BeritaModel extends CI_Model {

    //instance variable name for table

    function __construct() {
        parent::__construct();
    }

    function Add($data) {
        $this->db->insert('berita', $data);
        return $this->db->insert_id();
    }

    function Update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('berita', $data);
        return $id;
    }

    function GetLatestBerita($limit) {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->limit($limit);
        return $this->db->get();
    }

    function Delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('berita');
        return true;
    }

    function GetAllData($param) {
        $this->db->select('berita.*, users.firstname, users.lastname')
        ->from('berita')
        ->join('users','berita.author = users.id', 'left')
        ->order_by('created_at', 'DESC')
        ->where('param', $param);
        
        return $this->db->get();
    }

    function GetHeadlineData() {
        $this->db->select('berita.*, users.firstname, users.lastname')
        ->from('berita')
        ->join('users','berita.author = users.id', 'left')
        ->order_by('created_at', 'DESC')
        ->where('param', 'berita')
        ->limit(1);

        return $this->db->get();
    }

    function GetDataByID($id) {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function GetAllDataPage($params){
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->where('param', $params);
        return $this->db->get();
    }
}