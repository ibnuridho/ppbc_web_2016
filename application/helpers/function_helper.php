<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('getCounterOrder')) {

    function getCounterOrder() {
        $CI = & get_instance();
        $CI->load->database();
        $sql = "SELECT count(id) as totalOrder from `order` ";
        // echo $sql;die();
        $query = $CI->db->query($sql);
        $orders = $query->row();
        return $orders->totalOrder;   
    }
}


if (!function_exists('getCounterRequest')) {

    function getCounterRequest() {
        $CI = & get_instance();
        $CI->load->database();
        $sql = "SELECT count(id) as totalRequest from `trip_request` ";
        // echo $sql;die();
        $query = $CI->db->query($sql);
        $orders = $query->row();
        return $orders->totalRequest;   
    }
}
