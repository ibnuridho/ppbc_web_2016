<aside class="right-side">
    <section class="content-header">
        <h1>
            <a href="javascript:void(0)" onclick="AddNew()"><i class="fa fa-plus-square"></i> Add New</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Master</a></li>
            <li class="active"><a href="<?php echo site_url('master/account_bank'); ?>">Account Bank</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th>Bank Name</th>
                                    <th>Account Name</th>
                                    <th>Rekening Number</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($banks->num_rows > 0) {
                                    foreach ($banks->result() as $bank) {
                                        if ($bank->status == 1) {
                                            $status = '<label class="label label-info">Active</label>';
                                        } else {
                                            $status = '<label class="label label-warning">Deactive</label>';
                                        }
                                        ?>
                                        <tr class="bank-row-<?php echo $bank->id ?>">
                                            <td><?php echo $bank->bank_name; ?></td>
                                            <td><?php echo $bank->account_name; ?></td>
                                            <td><?php echo $bank->no_rekening; ?></td>
                                            <td><?php echo $status ?></td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="Edit(<?php echo $bank->id ?>)"><button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button></a>
                                                <a href="javascript:void(0)" onclick="confirm_delete(<?php echo $bank->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<!-- Modal Confirmasi Delete -->
<div class="modal fade" id="BankForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Add New</h4>
            </div>
            <div class="modal-body">
                <div class="box-body row">
                    <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="master_bank_error" style="display: none;">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                    </div>
                    <form id="bank_form" action="<?php echo $action_form ?>" class="">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Bank Name</label>
                                <div class="input-group">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                                    <input class="form-control" name="bank_name" id="bank_name" type="text" value="" placeholder="Bank Name ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Account Name</label>
                                <div class="input-group">
                                    <input class="form-control" name="account_name" id="account_name" type="text" value=""  placeholder="Account Name ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Rekening Number</label>
                                <div class="input-group">
                                    <input class="form-control timepicker" name= "no_rekening" id="no_rekening" type="text" value=""  placeholder="Rekening Number ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">- Choose State - </option>
                                    <option value="1"> Active </option>
                                    <option value="0"> Deactive </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Logo Bank</label>
                                <div class="input-group col-md-5">
                                    <input type="hidden" name="image" id="image">
                                    <a href="javascript:void(0)" class="thumbnail" onclick="UploadImage()">
                                        <img src="<?php echo base_url(); ?>assets/images/upload.png" id="img-uploader" alt="...">
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-yes pull-right"><i class="fa fa-save"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
            </div>
            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
        function confirm_delete(id) {
            $('#ConfirmDelete').modal('show');
            $('#yes-delete').click(function() {
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('master/DeleteAccountBank') ?>',
                    data: 'id=' + id,
                    dataType: 'json',
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#ConfirmDelete').modal('hide');
                            $('.bank-row-' + id).remove();
                        } else {
                            alert("Opration Failed !");
                        }
                    }
                })
                return false;
            });
        }

        function AddNew() {
            clearForm('bank_form');
            hideAlert();
            $('#BankForm').modal('show');
        }

        function Edit(id) {
            if(id) {
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('master/GetDataBankByID') ?>',
                    dataType: 'json',
                    data: 'id='+id,
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#BankForm').modal('show');
                            $('#bank_name').val(data.bank_name);
                            $('#account_name').val(data.account_name);
                            $('#no_rekening').val(data.no_rekening);
                            $('#status').val(data.status);
                            $('#image').val(data.image);
                            $('#action').val(data.action);
                            $('#id').val(id);
                            $('#img-uploader').attr('src','<?php echo base_url() ?>assets/images/logo_bank/'+data.image);
                        } else {
                            $('#master_bank_error').show();
                            $('#master_bank_error #alert_message').html(data.message);
                        }
                    }
                });
                return false;
            }
        }

        function UploadImage() {
            image = $('#image');
            var uploadLink = $('#img-uploader');
            var list_image = $('#list-image');
            new AjaxUpload(uploadLink, {
                action: '<?php echo site_url('upload/LogoBank') ?>',
                name: 'image',
                onSubmit: function(file, ext) {
                    if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                        $('.loader-page').fadeOut();
                        $('#image_error').show();
                        $('#image_error #alert_message').html('Only JPG, PNG or GIF files are allowed');
                        return false;
                    }
                }, onComplete: function(file, response) {
                    //On completion clear the status
                    var json = $.parseJSON(response);
                    //Add uploaded file to list
                    if (json.result === "success") {
                        $('#img-uploader').attr('src', '<?php echo base_url() ?>assets/images/logo_bank/' + json.filename);
                        $('#image').val(json.filename);
                    } else {
                        $('#master_bank_error').show();
                        $('#master_bank_error #alert_message').html('Failed to upload images.');
                    }
                }
            });
        }
        $(document).ready(function() {
            //form submit untuk form credential
            $("#bank_form").submit(function() {
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            window.location.href = data.redirect;
                        } else {
                            $('#master_bank_error').show();
                            $('#master_bank_error #alert_message').html(data.message);
                        }
                    }
                });
                return false;
            });
        });
</script>