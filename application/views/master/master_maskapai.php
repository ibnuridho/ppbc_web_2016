<aside class="right-side">
    <section class="content-header">
        <h1>
            <a href="javascript:void(0)" onclick="AddNew()"><i class="fa fa-plus-square"></i> Add New</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Master</a></li>
            <li class="active"><a href="<?php echo site_url('master/account_bank'); ?>">Account Bank</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th style="width:10%;">Thumbnail</th>
                                    <th style="width:25%;">Maskapai Name</th>
                                    <th style="width:50%;">Description</th>
                                    <th style="width:15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($maskapais->num_rows > 0) {
                                    foreach ($maskapais->result() as $maskapai) {
                                        ?>
                                        <tr class="maskapai-row-<?php echo $maskapai->id ?>">
                                            <td>
                                                <a href="javascript:void(0)" class="thumbnail" onclick="viewImageDetail('{{asset('img/uploads/community')}}/detail_{{$community->attachment->url}}')">
                                                    <img src="<?php echo base_url() ?>assets/images/maskapai/<?php echo $maskapai->image; ?>" class="table-thumbnail" style="width:100%;" alt="...">
                                                </a>
                                            </td>
                                            <td><?php echo $maskapai->name; ?></td>
                                            <td><?php echo $maskapai->description; ?></td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="Edit(<?php echo $maskapai->id ?>)"><button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button></a>
                                                <a href="javascript:void(0)" onclick="confirm_delete(<?php echo $maskapai->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<!-- Modal Confirmasi Delete -->
<div class="modal fade" id="MaskapaiForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Add New</h4>
            </div>
            <div class="modal-body">
                <div class="box-body row">
                    <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="master_bank_error" style="display: none;">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                    </div>
                    <form id="maskapai_form" action="<?php echo $action_form ?>" class="">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Maskapai Name</label>
                                <div class="input-group">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                                    <input class="form-control" name="name" id="name" type="text" value="" placeholder="Maskapai Name ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" rows="3" cols="" class="form-control" id="description" placeholder="Description ..."></textarea>
                            </div>
                            <div class="form-group">
                                <label>Maskapai Logo</label>
                                <div class="input-group">
                                    <input type="hidden" name="image" id="image">
                                    <a href="javascript:void(0)" class="thumbnail" onclick="UploadImage()">
                                        <img src="<?php echo base_url(); ?>assets/images/upload.png" id="img-uploader" alt="...">
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-yes btn-flat"><i class="fa fa-save"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
            </div>
            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
        $(function() {
            image = $('#image');
            var uploadLink = $('#img-uploader');
            var list_image = $('#list-image');
            new AjaxUpload(uploadLink, {
                action: '<?php echo site_url('upload/Maskapai') ?>',
                name: 'image',
                onSubmit: function(file, ext) {
                    if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                        $('.loader-page').fadeOut();
                        $('#image_error').show();
                        $('#image_error #alert_message').html('Only JPG, PNG or GIF files are allowed');
                        return false;
                    }
                }, onComplete: function(file, response) {
                    //On completion clear the status
                    var json = $.parseJSON(response);
                    //Add uploaded file to list
                    if (json.result === "success") {
                        $('#img-uploader').attr('src', '<?php echo base_url() ?>assets/images/maskapai/' + json.filename);
                        $('#image').val(json.filename);
                    } else {
                        $('#slider_error').show();
                        $('#slider_error #alert_message').html('Failed to upload images.');
                    }
                }
            });
        });
        function confirm_delete(id) {
            $('#ConfirmDelete').modal('show');
            $('#yes-delete').click(function() {
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('master/DeleteMaskapai') ?>',
                    data: 'id=' + id,
                    dataType: 'json',
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#ConfirmDelete').modal('hide');
                            $('.maskapai-row-' + id).remove();
                        } else {
                            alert("Opration Failed !");
                        }
                    }
                })
                return false;
            });
        }

        function AddNew() {
            clearForm('maskapai_form');
            hideAlert();
            $('#MaskapaiForm').modal('show');
        }

        function Edit(id) {
            if(id) {
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('master/GetDataMaskapaiByID') ?>',
                    dataType: 'json',
                    data: 'id='+id,
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#MaskapaiForm').modal('show');
                            $('#name').val(data.name);
                            $('#description').val(data.description);
                            $('#image').val(data.image);
                            $('#action').val(data.action);
                            $('#id').val(id);
                            $('#img-uploader').attr('src','<?php echo base_url() ?>assets/images/maskapai/'+data.image);
                        } else {
                            $('#master_bank_error').show();
                            $('#master_bank_error #alert_message').html(data.message);
                        }
                    }
                });
                return false;
            }
        }
        
        $(document).ready(function() {
            //form submit untuk form credential
            $("#maskapai_form").submit(function() {
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            window.location.href = data.redirect;
                        } else {
                            $('#master_bank_error').show();
                            $('#master_bank_error #alert_message').html(data.message);
                        }
                    }
                });
                return false;
            });
        });
</script>