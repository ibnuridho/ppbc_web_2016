<?php
if ($action == 'edit') {
    $username = $user->username;
    $firstname = $user->firstname;
    $lastname = $user->lastname;
    $email = $user->email;
    $image = $user->image;
    $status = $user->status;
    $id = $user->id;
    $title = '<i class="fa fa-edit"></i> Edit User';
    $btn_label = '<i class="fa fa-sign-in"></i> Edit User';
} else {
    $id = '';
    $username = '';
    $firstname = '';
    $lastname = '';
    $email = '';
    $image = 'no_image.jpg';
    $status = '';
    $title = '<i class="fa fa-plus-square-o"></i> Add New User';
    $btn_label = '<i class="fa fa-sign-in"></i> Save';
}
?>
<aside class="right-side">
    <section class="content">
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#add_new_user" data-toggle="tab"><h4> <?php echo $title ?></h4></a></li>
                </ul>
                <div class="tab-pane" id="add_new_user">
                    <div class="box-body row">
                        <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="user_error" style="display: none;">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                        </div>
                        <form id="user_form" action="<?php echo $action_form ?>" class="add-margin-left">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                                    <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                                    <input class="form-control" value="<?php echo $username ?>" name="username" id="username" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control" value="<?php echo $firstname ?>"  name="firstname" id="firstname" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" value="<?php echo $lastname ?>"  name="lastname" id="lastname" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="<?php echo $email ?>"  name="email" id="email" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">- Choose State - </option>
                                        <option value="1" <?php if ($status == 1) echo 'selected'; ?>> Active </option>
                                        <option value="0" <?php if ($status == 0) echo 'selected'; ?>> Deactive </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" value=""  name="password" id="password" type="password" value="">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control" name="confirm_password" id="confirm_password" type="password" value="">
                                </div>
<!--                                 <div class="form-group">
                                    <label class="control-label">Photo Profile</label>
                                    <a href="javascript:void(0)" class="thumbnail" onclick="UploadImage()"><div id="uploadLink" style="width:100px;"><img src="<?php echo base_url() ?>assets/images/<?php echo $image ?>" alt="Photo Profile" width="100"/></div></a>
                                    <input class="form-control" name="image" id="image" type="hidden" value="<?php echo $image; ?>">
                                </div> -->
                                <div class="form-group">
                                    <button class="btn bg-orange btn-flat" type="submit"><?php echo $btn_label; ?></button>&nbsp;&nbsp;<button class="btn bg-orange btn-flat"><i class="fa fa-rotate-left"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>

<script type="text/javascript">
        /*function UploadImage() {
            var uploadLink = $('#uploadLink');
            image = $('#image');
            new AjaxUpload(uploadLink, {
                action: '<?php echo site_url('upload/UploadProfileUser') ?>',
                name: 'image',
                onSubmit: function(file, ext) {
                    if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                        // extension is not allowed
                        alert('Only JPG, PNG or GIF files are allowed');
                        return false;
                    }
                }, onComplete: function(file, response) {
                    var json = $.parseJSON(response);
                    //Add uploaded file to list
                    if (json.result === "success") {
                        uploadLink.html('<img src="<?php echo base_url() ?>assets/images/' + json.filename + '" alt="Profile Photo" style="width : 100px"/> ');
                        image.val(json.filename);
                    } else {
                        uploadLink.html('<img src="<?php echo base_url('assets/images/no_image.jpg') ?>" title="Click to Change Image"  style="width : 100px"/> <br />Failed');
                        image.val(json.filename);
                    }
                }
            });
        }*/
        $(document).ready(function() {
            //form submit untuk form credential
            $("#user_form").submit(function() {
            $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            window.location.href = data.redirect;
                        } else {
                            $('#user_error').show();
                            $('#user_error #alert_message').html(data.message);
                        }
                    }
                });
                return false;
            });
        });
</script>