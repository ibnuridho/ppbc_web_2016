<aside class="right-side">
    <section class="content">
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#add_new_user" data-toggle="tab"><h4> <i class="fa fa-plus-square-o"></i> Add New User</h4></a></li>
                </ul>
                <div class="tab-pane" id="add_new_user">
                    <div class="box-body row">
                        <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="error_add" style="display: none;">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                        </div>
                        <form id="form_add_user" action="<?php echo $action_edit_user ?>" class="add-margin-left">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" value="<?php echo $user->username ?>" name="username" id="username" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control" value="<?php echo $user->firstname ?>"  name="firstname" id="firstname" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" value="<?php echo $user->lastname ?>"  name="lastname" id="lastname" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="<?php echo $user->email ?>"  name="email" id="email" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">- Choose State - </option>
                                        <option value="1"> Active </option>
                                        <option value="0"> Deactive </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" value=""  name="password" id="password" type="password" value="">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control" name="confirm_password" id="confirm_password" type="password" value="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Photo Profile</label>
                                    <a href="javascript:void(0)" onclick="UploadImage()"><div id="uploadLink" style="width:100px;"><img src="<?php echo base_url() ?>assets/img/no-image-profile.png" alt="Photo Profile" width="100"/></div></a>
                                    <input class="form-control" name="photo" id="photo" type="hidden" value="">
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <button class="btn bg-orange btn-flat" type="submit">Add</button><button class="btn bg-orange margin">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>

<script type="text/javascript">
    function UploadImage() {
        var uploadLink = $('#uploadLink');
        image = $('#image');
        new AjaxUpload(uploadLink, {
            action: '<?php echo site_url('upload/UploadImage') ?>',
            name: 'image',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                    // extension is not allowed
                    alert('Only JPG, PNG or GIF files are allowed');
                    return false;
                }
            }, onComplete: function(file, response) {
                var json = $.parseJSON(response);
                //Add uploaded file to list
                if (json.result === "success") {
                    uploadLink.html('<img src="<?php echo base_url() ?>assets/img/' + json.filename + '" alt="Profile Photo" style="width : 100px"/> ');
                    image.val(json.filename);
                } else {
                    uploadLink.html('<img src="<?php echo base_url('assets/img/no-profile-image.png') ?>" title="Click to Change Image"  style="width : 100px"/> <br />Failed');
                    image.val(json.filename);
                }
            }
        });
    }
    $(document).ready(function() {
        //form submit untuk form credential
        $("#form_edit_user").submit(function() {
            $('.loader-page').fadeIn();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                dataType: 'json',
                data: $(this).serialize(),
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        window.location.href = data.redirect;
                    } else {
                        $('#error_add').show();
                        $('#error_add #alert_message').html(data.message);
                    }
                }
            });
            return false;
        });
    });
</script>