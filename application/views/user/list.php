<aside class="right-side">
    <section class="content-header">
        <h1>
            <i class="fa fa-list-alt"></i> List All User
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>"><i class="fa fa-dashboard"></i> User</a></li>
            <li class="active"><a href="<?= site_url('user/list'); ?>">List User</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($users->num_rows > 0) {
                                    foreach ($users->result() as $user) {
                                        if ($user->status == 1) {
                                            $status = '<label class="label label-info">Active</label>';
                                        } else {
                                            $status = '<label class="label label-warning">Deactive</label>';
                                        }
                                        ?>
                                        <tr class="user-row-<?php echo $user->id ?>">
                                            <td><?php echo $user->username ?></td>
                                            <td><?php echo $user->firstname ?></td>
                                            <td><?php echo $user->lastname ?></td>
                                            <td><?php echo $user->email ?></td>
                                            <td><?php echo $status ?></td>
                                            <td> 
                                                <a href="<?php echo site_url('user/edit') . '/' . $user->id ?>"><button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button></a>
                                                <a href="javascript:void(0)" onclick="confirm_delete(<?php echo $user->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<script type="text/javascript">
        function confirm_delete(id) {
            $('#ConfirmDelete').modal('show');
            $('.btn-yes').click(function(){
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('user/delete') ?>',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#ConfirmDelete').modal('hide');
                            $('.user-row-'+id).remove();
                        } else {
                            alert("Opration Failed !");
                        }
                    }
                })
                return false;
            });
        }
</script>