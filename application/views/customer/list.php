<aside class="right-side">
    <section class="content-header">
        <h1>
            <i class="fa fa-list-alt"></i> List All Customer
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>"><i class="fa fa-dashboard"></i> Customer</a></li>
            <li class="active"><a href="<?= site_url('customer'); ?>">List Customer</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Company</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($customers->num_rows > 0) {
                                    foreach ($customers->result() as $customer) {
                                        if ($customer->active == 1) {
                                            $status = '<label class="label label-info">Active</label>';
                                        } else {
                                            $status = '<label class="label label-warning">Deactive</label>';
                                        }
                                        ?>
                                        <tr class="customer-row-<?php echo $customer->id ?>">
                                            <td><?php echo $customer->firstname ?></td>
                                            <td><?php echo $customer->lastname ?></td>
                                            <td><?php echo $customer->email ?></td>
                                            <td><?php echo $customer->phone ?></td>
                                            <td><?php echo $customer->company ?></td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="confirm_delete(<?php echo $customer->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<script type="text/javascript">
        function confirm_delete(id) {
            $('#ConfirmDelete').modal('show');
            $('.btn-yes').click(function(){
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('customer/delete') ?>',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#ConfirmDelete').modal('hide');
                            $('.customer-row-'+id).remove();
                        } else {
                            alert("Opration Failed !");
                        }
                    }
                })
                return false;
            });
        }
</script>