    <!DOCTYPE html>
<html class="bg-light-blue">
    <head>
        <meta charset="UTF-8">
        <title>PPBC Administrator | Log in</title>
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <!-- jQuery 2.0.2 -->

        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
        <![endif]-->
    </head>
    <body class="bg-light-blue">
        <div class="loader-page" style="display:none;"></div>
        <div class="form-box" id="login-box">
            <div class="header"><img src="<?php echo base_url() ?>assets/images/logo.png"></div>
            <form action="<?php echo site_url('login/LoginProcess') ?>" id="login-form" method="post">
                <div class="body bg-gray">
                    <div class="callout callout-danger hide" id="callout-login">
                        <h4><i class="fa fa-exclamation-triangle"></i> Alert !</h4>
                        <p id="message_content"></p>
                    </div>
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="User ID"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>
<!--                     <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div> -->
                </div>
                <div class="footer">
                    <button type="submit" class="btn bg-blue btn-block"><i class="fa fa-signin"> </i>Sign me in</button>
                    <!-- <p><a href="#">I forgot my password</a></p> -->
                </div>
            </form>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#username').focus();
                $('#login-form').submit(function() {
                    $('.loader-page').fadeIn();
                    var form_data = $(this).serialize();
                    $.ajax({
                        type: 'POST',
                        url: $(this).attr('action'),
                        data: form_data,
                        dataType: 'json',
                        success: function(data) {
                            if (data.error === 0) {
                                $('.loader-page').fadeOut();
                                window.location.href = data.redirect;
                            } else {
                                $('#register-loader').fadeOut();
                                $('.callout-danger').removeClass('hide');
                                $('.callout-danger #message_content').html(data.message);
                                $('.loader-page').fadeOut();
                            }
                        }
                    })
                    return false;
                });
            });
        </script>
    </body>
</html>