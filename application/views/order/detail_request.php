<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Request
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('dashboard/profile'); ?>"></a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_profile_info" data-toggle="tab"><h4> <i class="fa fa-info-circle"></i> Request Detail</h4></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_profile_info">
                        <div class="box-body row">
                            <div class="box-body table-responsive col-xs-7">
                                <table class="table table-hover">
                                    <tr>
                                        <td>Name</td>
                                        <td><?php echo $request->name ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><?php echo $request->phone ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $request->email ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td><?php echo $request->address ?></td>
                                    </tr>
                                    <tr>
                                        <td>Type Trip</td>
                                        <td><?php echo $request->type ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Peserta Liburan</td>
                                        <td><?php echo $request->member ?></td>
                                    </tr>
                                    <tr>
                                        <td>Teman Liburan</td>
                                        <td><?php echo $request->friend ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pesan</td>
                                        <td><?php echo $request->comment ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Pemberangkatan</td>
                                        <td><?php echo date('j M Y', strtotime($request->date)); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $request->email ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Request</td>
                                        <td><?php echo date('j M Y', strtotime($request->created_at)); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
        </div>
    </section><!-- /.col -->
</aside>
