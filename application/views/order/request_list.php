<aside class="right-side">
    <section class="content-header">
        <h1>
            <i class="fa fa-list-alt"></i> List All Request Trip
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('order/request'); ?>">List Request</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($requests->num_rows > 0) {
                                    foreach ($requests->result() as $order) {
                                        ?>
                                        <tr class="customer-row-<?php echo $order->id ?>">
                                            <td><?php echo $order->name ?></td>
                                            <td><?php echo $order->phone ?></td>
                                            <td><?php echo $order->email ?></td>
                                            <td><?php echo $order->address ?></td>
                                            <td>
                                                <a href="<?php echo site_url('order/request_detail/').'/'.$order->id ?>" ><button class="btn btn-primary btn-flat"><i class="fa fa-search-plus"></i></button></a>
                                                <a href="javascript:void(0)" onclick="confirm_delete(<?php echo $order->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<script type="text/javascript">
        function confirm_delete(id) {
            $('#ConfirmDelete').modal('show');
            $('.btn-yes').click(function(){
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('customer/delete') ?>',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#ConfirmDelete').modal('hide');
                            $('.customer-row-'+id).remove();
                        } else {
                            alert("Opration Failed !");
                        }
                    }
                })
                return false;
            });
        }
</script>