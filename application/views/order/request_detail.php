<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Request Detail
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('dashboard/order/request'); ?>">Request</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_profile_info" data-toggle="tab"><h4> <i class="fa fa-info-circle"></i> Order Detail</h4></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_profile_info">
                        <div class="box-body row">
                            <div class="box-body table-responsive col-xs-7">
                                <table class="table table-hover">
                                    <tr>
                                        <td>Type Trip</td>
                                        <td><?php echo strtoupper($detail->type); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Budget</td>
                                        <td><?php echo $detail->name ?></td>
                                    </tr>
                                    <tr>
                                        <td>Teman Liburan</td>
                                        <td><?php echo $detail->friend ?></td>
                                    </tr>
                                    <tr>
                                        <td>Destination</td>
                                        <td><?php echo $detail->destination ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Peserta</td>
                                        <td><?php echo $detail->member ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Pemberangkatan</td>
                                        <td><?php echo date('j M Y', strtotime($detail->date)); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Full Name</td>
                                        <td><?php echo $detail->name ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $detail->email ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><?php echo $detail->phone ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td><?php echo $detail->address ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pesan / Comment</td>
                                        <td><?php echo $detail->comment ?></td>
                                    </tr>
                                    <tr>
                                        <td>Date Request</td>
                                        <td>Pada, <?php echo date('j M Y', strtotime($detail->created_at)); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
        </div>
    </section><!-- /.col -->
</aside>
