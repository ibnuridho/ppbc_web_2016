<div class="pogoSlider" id="js-main-slider">
<?php 
		foreach ($sliders->result() as $slider) {
			?>
			
				<div class="pogoSlider-slide" data-transition="slideOverLeft" data-duration="10000" style="background-image:url(<?php echo base_url() ?>assets/images/slider/<?php echo $slider -> image; ?>);">
					<div class="pogoSlider-slide-element" data-delay="10000">
						<h3><?php echo $slider -> description;?> </h3>
						<a href="about.html" class="link1">Read More</a>
					</div>
				</div>
			<?php
			}
		?>
</div>

