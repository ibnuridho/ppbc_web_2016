<div class="main-content">
	<div class="container">
		<h2 class="title-page">Booking </h2>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<div class="contact-us" style="background:#FFF;">
			<div class="col-md-12">
				<div class="panel panel-proary">
					<div class="panel-heading"><i class="fa fa-shopping-cart"></i> Pemesanan <?php echo $detail->name; ?></div>
					<div class="panel-body">
					<div class="alert alert-danger alert-dismissable" id="book_error" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b><i class="fa fa-exclamation-triangle"></i> Alert!</b>
						<p id="message"></p>
					</div>
					<div class="alert alert-info alert-dismissable" id="book_success" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b><i class="fa fa-info"></i> Success!</b>
						<p id="message"></p>
					</div>
					<div class="alert alert-info alert-dismissable" id="book_error" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b><i class="fa fa-exclamation-triangle"></i> Success!</b>
						<p id="message"></p>
					</div>
					<form accept-charset="UTF-8" action="<?php echo site_url('customer/receiveBook'); ?>" class="simple_form new_user_basic" enctype="multipart/form-data" id="bookForm" method="post" novalidate="novalidate">
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Nama
							</label>
							<div>
								<input class="string email required form-control" required id="product_id" value="<?php echo $detail->name; ?>" name="trip_name" type="hidden">
								<input class="string email required form-control" required id="product_id" value="<?php echo $detail->id; ?>" name="product_id" type="hidden">
								<input class="string email required form-control" required id="name" name="name" placeholder="Enter name" type="text">
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Email
							</label>
							<div>
								<input class="string email required form-control" required id="email" name="email" placeholder="Enter email" type="email">
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> No. Telepon
							</label>
							<div>
								<input class="string email required form-control format-numeric" required id="phone" name="phone" placeholder="Enter phone" type="text">
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Alamat
							</label>
							<div>
								<textarea class="form-control" name="address" required id="address" placeholder="Alamat"></textarea>
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								Tanggal Keberangkatan
							</label>
							<div>
								<input class="string email required form-control pick-datepicker" required id="date" name="date" placeholder="Choose date" type="text">
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								Jumlah Orang
							</label>
							<div>
								<input class="string email required form-control format-numeric" required id="member_total" name="member_total" placeholder="Jumlah Orang" type="text">
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								Pesan
							</label>
							<div>
								<textarea class="form-control" name="content" required id="content" placeholder="Message"></textarea>
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							</div>
						</div>
						<!--<div class="form-group ">
							<div>
								<?php echo $captcha['image']; ?>
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							<!--</div>
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Captcha
							</label>
							<div>
								<input class="string email required form-control" required id="captcha" name="captcha" placeholder="Enter captcha" type="email">
								<!-- <p class="help-block">Lorem ipsum dolor sit amet</p> -->
							<!--</div>
						</div>-->
						<div class="form-group col-md-12" style="padding:12px;">
							<button type="submit" class="btn btn-primary"><i class="fa fa-envelope"></i> Send Message</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    //form submit untuk form credential
    $("#bookForm").submit(function() {
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                $('.loader-page').fadeOut();
                if (data.error === 0) {
                    $('#book_success').show();
                    $('input').val('');
                    $('textarea').val('');
                    $('#book_success #message').html(data.message);
                } else {
                    $('#book_error').show();
                    $('#book_error #message').html(data.message);
                }
            }
        });
        return false;
    });
});
</script>