<div class="main-content">
	<div class="container">
		<h2 class="title-page">Tiket Pesawat</h2>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<!-- <div class="row-fluid">
			<div class="wordl-image">
				<img src="<?php echo base_url() ?>assets/images/maskapai/<?php echo $setting->image; ?>" style="  width: 100%;">
			</div>
		</div> -->
		<div class="row-fluid" style="margin-bottom:20px;">
			<div class="prod-desc info-text">
				<div class="highlight"><?php echo $setting->description; ?></div>
			</div>
		</div>
		<div class="row products">
			<?php
			if ($maskapais->num_rows > 0) {
				foreach ($maskapais->result() as $maskapai) {
					?>
					<div class="col-md-3" style="margin-bottom:20px;">
						<div class="prod-wrap">
							<div class="img-box maskapai"><img src="<?php echo base_url()?>/assets/images/maskapai/<?php echo $maskapai->image; ?>" alt="<?php echo $maskapai->name; ?>" class="maskapai"/></div>
							<!-- <h2><?php echo $maskapai->name; ?></h2> -->
							<!-- <h3><i class="fa fa-clock-o"></i>17 Oktober - 23 November</h3> -->
							<!-- <p>
								<?php echo $maskapai->description ?>
							</p> -->
							<!-- <a href="<?php echo site_url('product/detail/'.$trip->slug) ?>">Selengkapnya <i class="fa fa-mail-forward"></i></a> -->
						</div>
					</div>
					<?php
					}
				}
				?>
		</div>
	</div>
</div>