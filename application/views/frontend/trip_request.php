<div class="main-content">
	<div class="container">
		<h2 class="title-page">Request For a Trip</h2>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 left-content">
				<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
					<?php
					if($sliders->num_rows > 0) {
						foreach ($sliders->result() as $image) {
							?>
							<img src="<?php echo base_url() ?>assets/images/slider/<?php echo $image->image ?>" data-thumb="<?php echo base_url() ?>assets/images/products/<?php echo $image->image ?>" title="#htmlcaption<?php echo $image->id;?>" alt=""/>
							<?php
						}
					}
					?>
					</div>
					<?php
					if($sliders->num_rows > 0) {
						foreach ($sliders->result() as $image) {
							?>
							<div id="htmlcaption<?php echo $image->id ?>" class="nivo-html-caption">
								<p><?php echo $image->description; ?></p>
							</div>
							<?php
						}
					}
					?>    
				</div>
			</div>
		</div>
		<div class="clear gap-content"></div>
		<div class="contact-us" style="background:#FFF;">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading"><i class="fa fa-envelope"></i> Masukan detail dari destinasi trip anda :</div>
					<div class="panel-body">
                    <div class="alert alert-danger alert-dismissable col-xs-11 add-margin-left" id="errorMessageChurchEvent" style="display: none;">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                    </div>
					<div class="alert alert-info alert-dismissable" id="request_success" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b><i class="fa fa-info"></i> Success!</b>
						<p id="alert_message"></p>
					</div>
					<?php
					if($messages) {
						?>
						<div class="alert alert-danger alert-dismissable" id="user_error" style="">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<b><i class="fa fa-exclamation-triangle"></i> Alert!</b>
							<?php
							print_r($messages);
							?>
						</div>	
						<?php
					}
					if($success) {
						?>
						<div class="alert alert-info alert-dismissable" id="user_error" style="">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<b><i class="fa fa-exclamation-triangle"></i> Success!</b>
							<?php
							print_r($success);
							?>
						</div>	
						<?php
					}
					?>
					<form accept-charset="UTF-8" action="<?php echo site_url('product/CustomeRequestProccess'); ?>" class="simple_form new_user_basic" enctype="multipart/form-data" id="requestTripForm" method="post" novalidate="novalidate">
						<div class="center-content">
							<div class="form-group">
								<label class="email required control-label" for="user_basic_email">
									<abbr title="required">*</abbr> Pilih jenis tempat yang ingin anda kunjungi
								</label>
							</div>
							<div class="row">
								<div class="col-md-6">
	                                <a href="javascript:void(0)" class="thumbnail type_urban" onclick="selectType('urban')" id="uploadLink">
	                                    <img src="<?php echo base_url() ?>assets/images/option1.jpg" id="img-uploader" alt="...">
	                                </a>
									<label class="email required control-label" for="user_basic_email">
										<input type="checkbox" name="urban" id="urban" value="urban">Urban
									</label>
								</div>
								<div class="col-md-6">
									<a href="javascript:void(0)" class="thumbnail type_nature" onclick="selectType('nature')" id="uploadLink">
	                                	<img src="<?php echo base_url() ?>assets/images/option2.jpg" id="img-uploader" alt="...">
	                                </a>
									<label class="email required control-label" for="user_basic_email" style="text-align:center;">
										<input type="checkbox" name="nature" id="nature" value="nature">Nature
									</label>
								</div>
							</div>
						</div>
						<div class="form-group ">
							<div class="col-md-12">
							<label class="email required control-label" for="user_basic_email">
								<br/><br/>
							</label>
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Berapa range budget liburan anda ?
							</label>
							<div>
								<input class="string numeric-value email required form-control" required id="budget" name="budget" placeholder="Enter your budget" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Bersama siapa anda liburan ?
							</label>
							<div>
								<input class="" required id="friend" value="Sendiri" name="friend"  type="radio"> Sendiri
								<input class="" required id="friend" value="Keluarga" name="friend"  type="radio"> Keluarga
								<input class="" required id="friend" value="Teman" name="friend"  type="radio"> Teman-teman
								<input class="" required id="friend" value="Pasangan" name="friend"  type="radio"> Pasangan
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Kemana destinasi liburan anda ?
							</label>
							<div>
								<input class="string email required form-control" required id="destination" name="destination" placeholder="Enter your destination" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								 Berapa jumlah orang yang akan berlibur ?
							</label>
							<div>
								<input class="string email required form-control numeric-value" required id="member" name="member" placeholder="" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Kapan tanggal berlibur anda ?
							</label>
							<div>
								<input class="string email required form-control pick-datepicker" required id="date" name="date" placeholder="" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Berapa lama rencana liburan anda ?
							</label>
							<div>
								<input class="string email required form-control" required id="duration" name="duration" placeholder="" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Masukan Nama Lengkap anda :
							</label>
							<div>
								<input class="string email required form-control" required id="name" name="name" placeholder="" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Masukan alamat lengkap anda :
							</label>
							<div>
								<textarea class="form-control" name="address" required id="address" placeholder="Address"></textarea>
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Masukan email anda :
							</label>
							<div>
								<input class="string email required form-control" required id="email" name="email" placeholder="" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Masukan Nomor Handphone anda :
							</label>
							<div>
								<input class="string email required numeric-value form-control" required id="phone" name="phone" placeholder="" type="text">
							</div>
						</div>
						<div class="form-group ">
							<label class="email required control-label" for="user_basic_email">
								<abbr title="required">*</abbr> Masukan pesan tambahan anda :
							</label>
							<div>
								<textarea class="form-control" name="comment" required id="comment" placeholder="Message"></textarea>
							</div>
						</div>
						<div class="form-group col-md-12" style="padding:12px;">
							<button type="submit" class="btn btn-primary"><i class="fa fa-envelope"></i> Send Request</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">

	function selectType(type) {
		// $('.thumbnail').removeClass('image-type-active');
		$('.type_'+type).addClass('image-type-active');
		$("input[name="+type+"][value=" + type + "]").prop('checked', true);
	}



    $(document).ready(function() {

    	$('#nature').change(function() {
	        if($(this).is(":checked")) {
	        	var value = $(this).val();
	        	$('.type_nature').addClass('image-type-active');
	        } else {
				$('.type_nature').removeClass('image-type-active');
	        }
	    });
    	$('#urban').change(function() {
	        if($(this).is(":checked")) {
	        	var value = $(this).val();
	        	$('.type_urban').addClass('image-type-active');
	        } else {
				$('.type_urban').removeClass('image-type-active');
	        }
	    });
    	//handle event change radio button untuk radio button
    	$('input[type=radio][name=friend]').change(function(){
    		var friend  = $("#friend:checked").val();
    		if(friend === 'Sendiri') {
    			$('#member').attr('disabled',true);
    		} else {
    			$('#member').attr('disabled',false);
    		}
    	});


    	$('input[type=radio][name=type]').change(function() {
	        if (this.value == 'urban') {
				$('.thumbnail').removeClass('image-type-active');
				$('.type_urban').addClass('image-type-active');
	        }
	        else if (this.value == 'nature') {
				$('.thumbnail').removeClass('image-type-active');
				$('.type_nature').addClass('image-type-active');
	        }
    	});

        $('#requestTripForm').submit(function() {
            $('.loader-page').fadeIn();
            var form_data = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: form_data,
                dataType: 'json',
                success: function(data) {
                    if (data.error === 0) {
	                    $('input').val('');
	                    $('textarea').val('');
	       				// alert(data.message);
	       				$('#request_success').show();
                        $('.alert-danger').hide();
						$('.thumbnail').removeClass('image-type-active');
	                    $('#request_success #alert_message').html(data.message);
                        // window.location.href = data.redirect;
                    } else {
                        $('.alert-danger').show();
                        $('.alert-danger #alert_message').html(data.message);
                    }
                }
            })
            return false;
        });
    });
</script>