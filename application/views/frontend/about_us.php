<div class="main-content">
	<div class="container">
		<h2 class="title-page">About ManaTrip</h2>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<div class="jumbotron">
			<p style="text-align:justify; font-size: 14px;line-height: 26px;">
				PPBC.org adalah sebuah usaha online yang mampu menyediakan sebuah cara baru yang praktis dan fleksibel bagi konsumen untuk merencanakan perjalanan ataupun liburan dengan ciri khasnya berupa penyediaan itinerary (rancangan jadwal perjalanan) dengan konsep free and easy.</p>
		</div>
	</div>
</div>