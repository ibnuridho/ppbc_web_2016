<!DOCTYPE html>
<html>
<head>
<title><?php echo $PageTitle; ?></title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="PPBC PPBC PERSATUAN PENSIUNAN BEA & CUKAI." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='<?php echo base_url() ?>assets/css/font/Ubuntu.css' rel='stylesheet' type='text/css'>
<link href='<?php echo base_url() ?>assets/css/font/Titillium_Web.css' rel='stylesheet' type='text/css'>
<!--link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"-->
<!-- /fonts -->
<!-- css files -->
<link href="<?php echo base_url() ?>assets/ppbc/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/pricetable.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/pogo-slider.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css files -->
<!-- js files -->
<script src="<?php echo base_url() ?>assets/ppbc/js/modernizr.js"></script>
<!-- /js files -->
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$this->load->view('frontend/header');
$this->load->view($page);
$this->load->view('frontend/footer');
?>
<!-- /back to top -->
<!-- js files -->
<script src="<?php echo base_url() ?>assets/ppbc/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/SmoothScroll.min.js"></script>

<!-- scroll to top -->
<script>
	$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $("section.footer a[href='#myPage']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
   if (this.hash !== "") {

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
    	scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
    } // End if
  });
})
</script>
<!-- /scroll to top -->
<script src="<?php echo base_url() ?>assets/ppbc/js/pricetable.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/wmBox.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/info.js"></script>
<!-- js for pricing table pop up -->
<script src="<?php echo base_url() ?>assets/ppbc/js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

	});
</script>
<!-- /js for pricing table pop up -->
<!-- js for back to top -->
<script src="<?php echo base_url() ?>assets/ppbc/js/top.js"></script>
<!-- /js for back to top -->
<!-- js for search button -->
<script src="<?php echo base_url() ?>assets/ppbc/js/classie.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/uisearch.js"></script>
<script>
	// new UISearch( document.getElementById( 'sb-search' ) );
</script>
<!-- /js for search button -->
<script src="<?php echo base_url() ?>assets/ppbc/js/jquery.pogo-slider.min.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/main.js"></script>
<!-- /js files -->
</body>
</html>