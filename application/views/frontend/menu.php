<section class="top-w3ls">
	<!-- top bar -->	
	<div class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 top-w3ls1">
					<p class="top-p1-lg">PERKUMPULAN PURNA BHAKTI BEA & CUKAI.</p>
				</div>
				<div class="col-lg-4 col-md-4 top-w3ls2 text-right">
					<ul class="top-contacts">
						<li class="top-hover"><p><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:support@company.com">support@ppbcpusat.org</a></p>
							<!-- <li class="top-unhover"><p><span class="glyphicon glyphicon-phone-alt"></span> </p> -->
						</ul>			
					</div>
				</div>
			</div>		
		</div>
		<!-- /top bar -->
		<!-- navigation -->
		<div class="navbar-wrapper">
			<div class="container">
				<nav class="navbar navbar-inverse navbar-static-top">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand logo-brand" href="<?php echo site_url('index'); ?>">
								<!-- <h1>Terrain</h1> -->
								<img src="<?php echo base_url() ?>assets/ppbc/images/logos/logo-for-header.png" alt="">
							</a>
						</div>
						<div id="navbar" class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
								<li class="active"><a href="<?php echo site_url('index'); ?>">Home</a></li>
								<li>
					                <div class='dropdown'>
					                  <button class='dropbtn'>Tentang PPBC</button>
					                  <div class='dropdown-content'>
					          			<a href='<?php echo site_url('index/tentang_kami/Visi Misi'); ?>'>Visi dan Misi</a>
					          			<a href='<?php echo site_url('index/tentang_kami/Struktur Organisasi'); ?>'>Struktur Organisasi</a>
					          			<a href='<?php echo site_url('index/tentang_kami/AD - ART'); ?>'>AD / ART</a>
					          			<a href='<?php echo site_url('index/tentang_kami/Sekilas PPBC'); ?>'>Sekilas PPBC</a>
					          			<a href='<?php echo site_url('index/tentang_kami/Tugas Pokok & Fungsi'); ?>'>Tugas Pokok & Fungsi</a>
					          			<a href='<?php echo site_url('index/tentang_kami/Logo PPBC'); ?>'>Logo PPBC</a>
					          			<a href='<?php echo site_url('index/tentang_kami/Mars PPBC'); ?>'>Mars PPBC</a>
					            	  </div>
					                </div>
					            </li>
								<li><a href="<?php echo site_url('index/faq'); ?>">Kegiatan</a></li>
								<li><a href="<?php echo site_url('index/agenda'); ?>">Agenda</a></li>
								<li><a href="<?php echo site_url('index/faq'); ?>">Berita</a></li>
								<li><a href="<?php echo site_url('index/data_anggota'); ?>">Data Keanggotan</a></li>
								<li><a href="<?php echo site_url('index/contact'); ?>">Kontak Kami</a></li>
							</ul>
						</div>
					</div>
				</nav>	
		<!-- <div class="search-box">
			<div id="sb-search" class="sb-search">
				<form action="#" method="post">
					<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
					<input class="sb-search-submit" type="submit" value="">
					<span class="sb-icon-search"> </span>
				</form>
			</div>
		</div> -->
	</div>
</div>
</section>	