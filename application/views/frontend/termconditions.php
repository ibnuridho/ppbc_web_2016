<div class="main-content">
	<div class="container">
		<h2 class="title-page">Syarat dan Ketentuan</h2>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<div class="jumbotron">
			<p style="text-align:justify; font-size: 15px !important;line-height: 26px;">
				<!-- <h3>Apa itu PPBC.org?</h3> -->
				<ol type="1">
					<li><strong>Pendahuluan</strong>
						<ol type="a">
							<li>Terima kasih atas kunjungan Anda ke Website Kami, http://www.PPBC.org. Kami berharap agar kunjungan Anda dapat bermanfaat dan memberi kenyamanan dalam mengakses dan menggunakan seluruh Layanan yang tersedia di Website Kami. Kami terus menerus berupaya memperbaiki dan meningkatan mutu pelayanan Kami, dan sangat menghargai segala kritik, saran dan masukan dari Anda; Silakan Anda menyampaikannya ke Kami melalu cs_manatrip@gmail.com atau telepon di +6283187565695 antara jam 08:00 sampai dengan 21:00 Waktu Indonesia Bagian Barat.</li>
							<li>Website dan Layanan Kami tersedia secara online melalui Website: www.PPBC.org atau berbagai akses, media, perangkat dan platform lainnya, baik yang sudah atau akan tersedia dikemudian hari.</li>
						</ol>
					</li>
					<li><strong>Umum</strong>
						<ol type="a">
							<li>Dengan mengakses dan menggunakan Website dan Layanan Kami, Anda menyatakan telah membaca, memahami, menyetujui dan menyatakan tunduk pada Syarat dan Ketentuan Penggunaan PPBC.org ini. Jika Anda tidak dapat menyetujui Syarat dan Ketentuan Penggunaan PPBC.org ini, baik secara keseluruhan ataupun sebagian, maka Anda tidak diperbolehkan untuk mengakses Website ini ataupun menggunakan Layanan yang Kami sediakan.</li>
							<li>Syarat dan Ketentuan Penggunaan PPBC.org ini terdiri atas (i) syarat dan ketentuan umum yang berlaku untuk setiap akses dan Layanan yang tersedia pada Website, dan (ii) syarat dan ketentuan khusus yang mengatur lebih lanjut ketentuan penggunaan produk atau Layanan tertentu. Dalam hal ditemukan adanya perbedaan atau pertentangan antara syarat dan ketentuan umum dan syarat dan ketentuan khusus, maka yang berlaku adalah syarat dan ketentuan khusus.</li>
							<li>Syarat dan Ketentuan Penggunaan PPBC.org dapat Kami ubah, modifikasi, tambah, hapus atau koreksi ("perubahan") setiap saat dan setiap perubahan itu berlaku sejak saat Kami nyatakan berlaku atau pada waktu lain yang ditetapkan oleh Kami; Anda Kami anjurkan untuk mengunjungi Website Kami secara berkala agar dapat mengetahui adanya perubahan tersebut.</li>
						</ol>
					</li>
					<li>
						<strong>Penggunaan</strong>
						<ol type="a">
							<li>Website ini dan Layanan yang tersedia didalamnya dapat digunakan oleh Anda hanya untuk penggunaan pribadi dan secara non-komersial dan setiap saat tunduk pada dan berlaku syarat dan ketentuan yang saat itu berlaku dalam Syarat dan Ketentuan Penggunaan PPBC.org.</li>
							<li>Website ini dan produk-produk, teknologi dan proses yang terdapat atau terkandung dalam Website, dimiliki oleh Kami atau pihak ketiga yang memberi hak kepada Kami. Kecuali untuk penggunaan yang secara tegas diijinkan dan diperbolehkan dalam Syarat dan Ketentuan Penggunaan PPBC.org ini, Anda tidak memiliki ataupun menerima dan PPBC.org tidak memberikan hak lain apapun ke Anda atas Website ini, berikut dengan segala data, informasi dan konten didalamnya.</li>
							<li>Dengan menggunakan Website ini atau Layanan yang tersedia didalamnya, maka Anda menyatakan setuju tidak akan men-download, menayangkan atau mentransmisi dengan cara apa pun, dan atau membuat konten apa pun tersedia untuk umum yang tidak konsisten dengan penggunaan yang diijinkan dalam Syarat dan Ketentuan Penggunaan PPBC.org ini.</li>
							<li>Dalam Website ini mungkin terdapat link (tautan) ke website yang dikelola oleh pihak ketiga ("Situs Eksternal"). Situs Eksternal disediakan hanya untuk referensi dan kenyamanan saja. PPBC.org tidak mengoperasikan, mengendalikan atau mendukung dalam bentuk apa pun Situs Eksternal yang bersangkutan ataupun konten/isinya. Anda bertanggung jawab penuh atas penggunaan Situs Eksternal tersebut dan dianjurkan untuk mempelajari syarat dan ketentuan dari Situs Eksternal itu secara seksama.</li>
							<li>Anda tidak boleh membuat link, melakukan screen capture atau data crawling ke Website tanpa adanya persetujuan tertulis sebelumnya dari PPBC.org. Hal-hal tersebut dianggap sebagai pelanggaran hak milik intelektual PPBC.org.</li>
						</ol>
					</li>
					<li><strong>Layanan PPBC.org</strong>
							<ol type="a">
								<li>PPBC.org menyediakan dan menyelenggarakan sistem dan fasilitas pemesanan online secara terpadu ("Layanan"), yang dapat melayani pemesanan/pemesanan: TRANSPORTASI (udara), yang memungkinkan Anda untuk mencari informasi atas Produk yang Anda inginkan, serta melakukan pemesanan paket-paket perjalanan yang telah disediakan.</li>
								<li>Layanan Kami secara umum dapat tersedia secara online selama dua puluh empat jam sehari dan tujuh hari dalam seminggu; kecuali dalam hal adanya perbaikan, peningkatan atau pemeliharaan pada Website Kami.</li>
								<li>Produk disediakan, disuplai dan diselenggarakan oleh pihak ketiga ("Mitra") yang telah mengadakan kerjasama dan telah mengadakan ikatan, baik secara langsung ataupun tidak langsung, dengan Kami. Anda memahami dan mengakui bahwa:
									<ol type="i">
										<li>Pemesanan dan pembelian yang Anda lakukan melalui PPBC.org, merupakan hubungan hukum dan kontrak yang mengikat antara Anda dan Mitra Kami. Dalam hal ini, PPBC.org bertindak sebagai agen atau perantara yang bertugas untuk memfasilitasi transaksi antara antara Anda dan mitra Kami.</li>
										<li>Data dan informasi terkait dengan Produk tertentu yang Kami cantumkan pada Website merupakan data dan informasi yang Kami terima dari Mitra, dan Kami mempublikasikan data dan informasi tersebut dengan itikad baik sesuai dengan data dan informasi yang Kami terima.</li>
										<li>Kami tidak memiliki kendali atas data dan informasi yang diberikan oleh Mitra Kami, dan Kami tidak menjamin bahwa data dan informasi yang disajikan adalah akurat, lengkap, atau benar, dan terbebas dari kesalahan.</li>
									</ol>
								<li>Anda tidak diperbolehkan untuk menjual kembali Produk Kami, menggunakan, menyalin, mengawasi, menampilkan, men-download, atau mereproduksi konten atau informasi, piranti lunak, atau Layanan apa pun yang tersedia di Website Kami untuk kegiatan atau tujuan komersial apapun, tanpa persetujuan tertulis dari Kami sebelumnya.</li>
								<li>Anda dapat menggunakan Website dan Layanan yang tersedia untuk membuat pemesanan/pemesanan yang sah. Anda tidak diperbolehkan untuk membuat pemesanan/pemesanan untuk tujuan spekulasi, tidak benar atau melanggar hukum. Jika Kami menemukan atau sewajarnya menduga bahwa pemesanan/pemesanan yang Anda buat ternyata tidak sah, maka Kami mencadangkan hak untuk membatalkan pemesanan/pemesanan Anda.</li>
								<li>Anda juga menjamin bahwa data dan informasi yang Anda berikan ke Kami, baik sehubungan dengan pemesanan/pemesanan ataupun pendaftaran pada PPBC.org, adalah data dan informasi yang akurat, terkini dan lengkap. Untuk ketentuan penggunaan data dan informasi yang Anda berikan, silakan untuk merujuk pada Kebijakan Penggunaan Data.</li>
							</ol>
						</li>
					</li>
					<li><strong>Pemesanan / Pembelian Produk</strong>
						<ol type="a">
							<li>Pemesanan/pembelian Produk dianggap berhasil atau selesai setelah Anda melakukan pelunasan pembayaran dan PPBC.org menerbitkan dan mengirim ke Anda, Surat Konfirmasi pemesanan/pembelian. Apabila terjadi perselisihan atau permasalahan, maka data yang terdapat pada PPBC.org akan menjadi acuan utama dan diangap sah.</li>
							<li>Dengan menyelesaikan pemesanan/pembelian, maka Anda dianggap setuju untuk menerima: (i) email yang akan Kami kirim tidak lama sebelum tanggal pelayanan yang Anda pesan, memberikan Anda informasi tentang Produk yang Anda beli, dan menyediakan Anda informasi dan penawaran tertentu (termasuk penawaran pihak ketiga yang telah Anda pilih sendiri) yang terkait dengan pemesanan dan tujuan Anda, dan (ii) email yang akan Kami kirim tidak lama setelah tanggal pelayanan untuk mengundang Anda untuk melengkapi formulir ulasan pengguna Produk Kami. Selain dari konfirmasi email yang menyediakan konfirmasi pemesanan dan email-email yang telah Anda pilih sendiri, Kami tidak akan mengirimi Anda pemberitahuan (yang diinginkan maupun yang tidak), email, korespondensi lebih lanjut, kecuali jika diminta secara khusus oleh Anda.</li>
							<li>Untuk jalur penerbangan internasional, pelanggan harus memiliki paspor dengan masa berlaku sekurang - kurangnya 6 (enam) bulan, pada saat tanggal keberangkatan dan/atau kepulangan.</li>
						</ol>
					</li>
					<li><strong>Harga Produk</strong>
						<ol type="a">
							<li>Kami selalu berupaya untuk menyediakan harga terbaik atas Produk untuk dapat dipesan oleh Anda. Harga yang tertera mungkin memiliki syarat dan ketentuan khusus, jadi Anda harus memeriksa sendiri dan memahami syarat dan ketentuan khusus yang berlaku terhadap suatu harga atau tarif tertentu sebelum Anda melakukan pemesanan. Anda juga perlu memeriksa dan memahami ketentuan mengenai pembatalan dan pengembalian dana yang secara khusus berlaku untuk Produk dan/atau Harga tertentu.</li>
							<li>Harga yang tercantum belum termasuk pajak, pungutan, biaya dan ongkos lainnya yang akan Kami uraian secara tegas pada Website atau Surat Konfirmasi dari Kami. Terkadang harga yang Kami peroleh dari Mitra Kami dalam mata uang Negara lain, dan Kami berupaya untuk mengkonversi harga tersebut ke dalam mata uang rupiah, dengan kurs konversi yang terbaik yang dapat Kami peroleh.</li>
							<li>Untuk Produk-Produk tertentu Kami juga memberikan jaminan harga terbaik bagi Anda. Harap Anda dapat mempelajari kebijaksanaan Kami atas jaminan harga terbaik Kami. Dalam kaitannya dengan kebijakan Kami atas jaminan harga terbaik itu, jika Anda menemukan ada harga yang lebih rendah di Layanan online lain di internet, harap Anda dapat memberitahu ke Kami ([cs@PPBC.org]) dan Kami akan berupaya untuk menyamakan harga/tarif Kami dengan harga/tarif lebih rendah yang Anda temukan.</li>
							<li>PPBC.org berhak untuk mengubah harga suatu produk setiap saat tanpa pemberitahuan sebelumnya, namun tetapi Produk yang sudah dibeli oleh Anda dan yang untuk Produk mana Anda sudah mendapat Surat Konfirmasi tidak akan berubah.</li>
						</ol>
					</li>
					<li><strong>Pembayaran</strong>
						<ol type="a">
							<li>Pelunasan atas harga pembelian merupakan syarat untuk melakukan pembelian. Kami menerima pembayaran dengan sistem pembayaran menggunakan transfer antar rekening serta antar bank ke rekening PPBC.org di bank-bank yang tercantum di Website.</li>
							<li>Atas setiap pemesanan yang dapat Kami konfirmasi, Kami akan mengirim Anda Surat Konfirmasi via email yang berisi uraian produk dan pemesanan yang Anda buat serta konfirmasi pembayaran. Anda bertanggung-jawab untuk mencetak dan menjaga informasi yang tertera pada Surat Konfirmasi yang Kami kirim. Surat konfirmasi ini merupakan dokumen yang sangat penting dan Anda wajib membawa cetakan dari Surat Konfirmasi ini pada saat Anda akan menggunakan atau mengambil Produk yang Anda beli. Kami atau Mitra Kami berhak untuk menolak memberikan Produk atau pelayanan, jika Anda tidak dapat membuktikan bahwa Anda telah secara sah melakukan pemesanan dan pelunasan, dan Anda membebaskan PPBC.org dari segala tanggung-jawab dan kerugian Anda dalam bentuk apapun.</li>
						</ol>
					</li>
					<li><strong>Perubahan dan Pembatalan</strong>
						<ol type="a">
							<li>Kecuali secara tegas dinyatakan lain dalam Syarat dan Ketentuan Penggunaan PPBC.org ini, semua pembelian Produk di PPBC.org tidak dapat diubah, dibatalkan, dikembalikan uang, ditukar atau dialihkan ke orang/pihak lain.</li>
							<li>Dengan melakukan pemesanan atau pembelian Produk di PPBC.org, Anda dianggap telah memahami, menerima dan menyetujui kebijakan dan ketentuan pembatalan, serta segala syarat dan ketentuan tambahan yang diberlakukan oleh Mitra. Kami akan mencantumkan kebijakan dan ketentuan pembatalan tersebut di setiap Surat Konfirmasi yang Kami kirim ke Anda. Harap dicatat bahwa tarif atau penawaran tertentu tidak memenuhi syarat untuk pembatalan atau pengubahan. Anda bertanggung-jawab untuk memeriksa dan memahami sendiri kebijakan dan ketentuan pembatalan tersebut sebelumnya.</li>
							<li>Jika Anda ingin melihat ulang, melakukan perubahan, atau membatalkan pesanan Anda, harap merujuk pada Surat Konfirmasi dan ikuti instruksi di dalamnya. Harap dicatat bahwa Anda mungkin saja dikenakan biaya tambahan atas pembatalan sesuai dengan kebijakan dan ketentuan pembatalan.</li>
							<li>Walaupun sangat kecil kemungkinan Kami membatalkan atau mengubah pemesanan yang sudah Kami konfirm dalam Surat Konfirmasi, namun jika diperlukan, Kami akan memberitahu Anda secepat mungkin.
								<ol type="i">
									<li>Kami akan bertanggung-jawab terhadap perubahan yang secara signifikan berpengaruh pada ketentuan Produk dan Layanan yang telah Kami konfirmasikan sebelumnya, yaitu:
										<ul>
											<li>Pembatalan pemesanan;</li>
											<li>Perubahan tanggal, atau Produk;</li>
										</ul>
									</li>
									<li>Dalam hal Kami melakukan perubahan signifikan, maka Anda memiliki pilihan untuk:
										<ul>
											<li>Menerima perubahan yang Kami tawarkan untuk tanggal atau Produk lain; atau</li>
										</ul>
									</li>
								</ol>
							</li>
							<li>Kami tidak bertanggung-jawab ataupun menanggung kerugian Anda dalam hal Kami tidak dapat menyerahkan Produk atau memberi Layanan kepada Anda, akibat dari hal-hal yang terjadi akibat keadaan memaksa atau yang diluar kekuasaan Kami atau Mitra Kami untuk mengendalikan, seperti, tapi tidak terbatas pada: perang, kerusuhan, teroris, perselisihan industrial, tindakan pemerintah, bencana alam, kebakaran atau banjir, cuaca ekstrim, dan lain sebagainya.</li>
						</ol>
					</li>
					<li>
						<strong>Keamanan</strong>
						<ol type="a">
							<li>Walaupun PPBC.org akan menggunakan upaya terbaik untuk memastikan keamanannya, PPBC.org tidak bisa menjamin seberapa kuat atau efektifnya ini dan PPBC.org tidak dan tidak akan bertanggung jawab atas masalah yang terjadi akibat pengaksesan tanpa ijin dari informasi yang Anda sediakan.</li>
						</ol>
					</li>
					<li><strong>Kebijakan Penggunaan Data</strong>
						<ol type="a">
							<li>Kami menganggap privasi Anda sebagai hal yang penting.</li>
							<li>Pada saat Anda membuat pemesanan di PPBC.org, Kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Pada prinsipnya, data Anda akan Pada saat Anda membuat pemesanan di PPBC.org, Kami akan mencatat dan menyimpan informasi dan data pribadi Anda. Pada prinsipnya, data Anda akan Kami gunakan untuk menyediakan Produk dan memberi Layanan kepada Anda. Kami akan menyimpan setiap data yang Anda berikan, dari waktu ke waktu, atau yang Kami kumpulkan dari penggunaan Produk dan Layanan Kami. Data pribadi Anda yang ada pada Kami, dapat Kami gunakan untuk keperluan akuntansi, tagihan, audit, verifikasi kredit atau pembayaran, serta keperluan security, administrasi dan legal, reward points atau bentuk sejenisnya, pengujian, pemeliharaan dan pengembangan sistem, hubungan pelanggan, promosi, dan membantu Kami dikemudian hari dalam memberi pelayanan kepada Anda. Sehubungan dengan itu, Kami dapat mengungkapkan data Anda kepada group perusahaan di mana PPBC.org tergabung didalamnya, Mitra penyedia Produk, perusahaan lain yang merupakan rekanan dari PPBC.org, perusahaan pemroses data yang terikat kontrak dengan Kami, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang, di jurisdiksi manapun.</li>
						</ol>
					</li>
					<li><strong>Penolakan Tanggung-Jawab</strong>
						<ol type="a">
							<li>Seluruh data, informasi, atau konten dalam bentuk apapun yang tersedia pada Website ini disediakan seperti apa adanya dan tanpa ada jaminan.</li>
							<li>Anda mengakui, setuju dan sepakat bahwa Anda menanggung sendiri segala bentuk resiko atas penggunaan Website dan Layanan. Lebih lanjut, Anda mengakui, setuju dan sepakat bahwa PPBC.org, termasuk setiap direktur, pejabat, pegawai, Mitra dan pihak lain manapun yang bekerjasama dengan PPBC.org, tidak bertanggung-jawab atas dan tidak memberi jaminan terhadap:
								<ol type="i">
									<li>Segala hal yang berkaitan dengan Website ini termasuk tapi tidak terbatas pada pengoperasian atau keakurasian data, kelayakan, kelengkapan data.</li>
									<li>baik yang tersirat maupun tersurat, termasuk jaminan yang tersirat dari pembelian atau kepatutan dari tujuan tertentu atau kelayakan untuk diperdagangkan;</li>
									<li>Kehilangan atau kerusakan, baik langsung, tidak langsung, khusus, yang bersifat konsekuensial atau termasuk kehilangan keuntungan, reputasi, kerusakan/kehilangan data, kerusakan koneksi yang tak dapat diperbaiki akibat penggunaan atau ketidakmampuan menggunakan Website ini baik yang berdasarkan hukum atau hal lain bahkan ketika kita diinformasikan mengenai kemungkinan-kemungkinan tersebut;</li>
									<li>Akses Anda terhadap Website, berikut dengan kerusakan yang mungkin timbul akibat akses Anda ke Website atau situs eksternal. Akses tersebut merupakan tanggung-jawab Anda sendiri dan Anda sendiri yang harus memastikan bahwa Anda terbebas dan terlindungi dari virus atau hal lain yang mungkin mengganggu atau merusak pengoperasian sistem komputer Anda.</li>
								</ol>
							</li>
							<li>Sejauh yang dimungkinkan secara hukum, tanggung-jawab dan kerugian yang dapat ditanggung oleh PPBC.org, baik untuk satu kejadian ataupun serangkaian kejadian yang saling terhubung, yang timbul dari kerugian yang secara langsung diderita oleh Anda, sebagai akibat dari kesalahan PPBC.org, adalah terbatas sampai dengan jumlah total biaya yang telah Anda bayar lunas sebagaimana tercantum dalam Surat Konfirmasi.</li>
						</ol>
					</li>
					<li><strong>Lain-lain</strong>
						<ol type="a">
							<li>Versi asli berbahasa Indonesia dari Syarat dan Ketentuan Penggunaan PPBC.org ini dimungkinkan untuk diterjemahkan ke bahasa-bahasa lain. Versi terjemahan dibuat hanya untuk memberi kemudahan saja, dan tidak bisa dianggap sebagai terjemahan resmi. Jika ditemukan adanya perbedaan antara versi bahasa Indonesia dan versi bahasa lainnya dari syarat dan ketentuan ini, maka yang berlaku dan mengikat adalah versi bahasa Indonesia.</li>
							<li>Website ini dibuat dan dikontrol oleh PPBC.org di Indonesia dan oleh dengan demikian tunduk dan diatur atas dasar hukum Indonesia. Menyangkut Syarat dan Ketentuan Penggunaan PPBC.org ini dan segala konsekuensinya, Anda dan Kami memilih domisili hukum yang umum dan tetap.</li>
							<li>Apabila ada bagian tertentu dari Syarat dan Ketentuan Penggunaan PPBC.org ini yang dianggap tidak sah atau tidak dapat berlaku, maka bagian tertentu ketentuan itu dianggap terhapus dan ketentuan lainnya tetap berlaku dan mengikat.</li>
						</ol>
					</li>
				</ol>
		</div>
	</div>
</div>