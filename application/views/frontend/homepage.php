<!DOCTYPE html>
<html>
<head>
<title><?php echo $PageTitle; ?></title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="PPBC PPBC PERSATUAN PENSIUNAN BEA & CUKAI." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='<?php echo base_url() ?>assets/css/font/Ubuntu.css' rel='stylesheet' type='text/css'>
<link href='<?php echo base_url() ?>assets/css/font/Titillium_Web.css' rel='stylesheet' type='text/css'>
<!--link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"-->
<!-- /fonts -->
<!-- css files -->
<link href="<?php echo base_url() ?>assets/ppbc/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/pricetable.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/pogo-slider.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url() ?>assets/ppbc/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css files -->
<!-- js files -->
<script src="<?php echo base_url() ?>assets/ppbc/js/modernizr.js"></script>
<!-- /js files -->
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
  <?php
  $this->load->view('frontend/menu');
  $this->load->view('frontend/slider');
  ?>

	<section class="welcome">
		<div class="container">	
			<div class="wel-w3ls">
				<h3 class="text-center w3layouts w3 w3l w3ls">Selamat Datang di PPBC</h3>
				<p class="text-center w3layouts w3 w3l w3ls">Setiap Pegawai Negeri Sipil (PNS) Republik Indonesia mempunyai batas usia pensiun dalam pengabdian tugasnya nya kepada Negara dan Bangsa yang diatur dengan  ketentuan Pemerintah.
Bahwa Pensiunan Bea dan Cukai adalah PNS yang telah diberi anugerah Tuhan Yang Maha Kuasa dapat menjalankan tugas nya sampai selesai pada usia yang ditetapkan sesuai  ketentuan Peraturan Pemerintah Republik Indonesia.
Bahwa Sesungguhnya  usia  lanjut  adalah  suatu  anugerah  dari  Tuhan  Yang Maha Kuasa yang perlu disyukuri oleh manusia lanjut usia, dengan jalan lebih mendekatkan  diri kepada Tuhan, mempertebal iman dan ketakwaan kepada perintah-perintahNya serta berbuat banyak kebajikan terhadap sesama atas dasar keikhlasan dan cinta kasih.
				<a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
				</p>
			</div>
		</div>	
	</section>
	
	<section class="team">
		<div class="container">
			<h3 class="text-center agileits agileinfo wthree w3-agileits">Berita</h3>
			<?php 
			if ($headline->num_rows > 0) {
				foreach ($headline->result() as $headlineData) {
			?>
				<div class="row">
		            <div class="col-md-5">
		                <a href="blog-post.html">
							<div class="hover01 column">
								<div>
									<figure>
									<img src="<?php echo base_url() ?>assets/ppbc/images/banner2.jpg" alt="" class="detail"/>
									</figure>
								</div>
							</div>
		                </a>
		            </div>
		            <div class="col-md-6" style="text-align: left;">
		                <h3>
		                    <a href="blog-post.html"><?php echo strtoupper($headlineData->judul) ?></a>
		                </h3>
		                <p class="blog-agile1 w3layouts w3 w3l w3ls">by <a href="https://w3layouts.com/"> 
		                <?php echo $headlineData->firstname . ' ' . $headlineData->lastname ?></a>  at 
		                <?php echo date('d M Y h:i:s' ,strtotime($headlineData->created_at)) ?>
		                </p>
		                <p class="blog-agile2 agileits agileinfo wthree w3-agileits">
		                	<?php echo $headlineData->prolog ?>
		                </p>
		                <hr>
		                <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
		            </div>
		        </div>
	        <?php
	    		}
	   		}
	        ?>
			<hr>
	        <div class="row">
				<?php
				foreach ($beritas->result() as $berita) {
				?>
				<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls" style="text-align: left">
					<div class="">
						<h3>
		                    <a href="blog-post.html"><?php echo strtoupper($berita->judul) ?></a>
		                </h3>
		                <p class="blog-agile1 w3layouts w3 w3l w3ls">by <a href="https://w3layouts.com/">
		                	<?php echo $berita->firstname . ' ' . $berita->lastname ?></a>  at 
		                	<?php echo date('d M Y h:i:s' ,strtotime($berita->created_at)) ?>
		                </a></p>
		                <p class="blog-agile2 agileits agileinfo wthree w3-agileits">
		                	<?php echo strtoupper($berita->prolog) ?>
		                </p>
		                <hr>
		                <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
		<hr>	
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h3 class="agileits agileinfo wthree w3-agileits">Berita Kesehatan Anggota</h3>
					<hr>	
					<?php 	
					foreach ($beritadukas->result() as $beritaduka) {
					?>
							<h3>
		                    <a href="blog-post.html"><?php echo strtoupper($beritaduka->judul) ?></a>
			                </h3>
			                <p class="blog-agile2 agileits agileinfo wthree w3-agileits">
			                	<?php echo $beritaduka->prolog ?>
			                </p>
			                <br>	
					<?php
					}
					?>
				</div>
				<div class="col-md-6">
					<h3 class="agileits agileinfo wthree w3-agileits">Berita Duka</h3>
					<hr>	
					<?php 	
					foreach ($beritadukas->result() as $beritaduka) {
					?>
							<h3>
		                    <a href="blog-post.html"><?php echo strtoupper($beritaduka->judul) ?></a>
			                </h3>
			                <p class="blog-agile2 agileits agileinfo wthree w3-agileits">
			                	<?php echo $beritaduka->prolog ?>
			                </p>
			                <br>	
					<?php
					}
					?>
				</div>
			</div>
		</div>

	</section>

	<section class="info">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 info-w3ls1">
					<a class="wmBox" href="#" data-popup="https://player.vimeo.com/video/33812159?title=0&byline=0&portrait=0">
						<img src="<?php echo base_url() ?>assets/ppbc/images/banner2.jpg" alt="w3layouts" title="w3layouts" class="img-responsive">
						<div class="b-wrapper">
							<i class="fa fa-play-circle-o" aria-hidden="true"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-7 col-md-7 info-w3ls2">
					<div class="info-agile">
						<h2>Kegiatan - Kongres I PPBC 2014</h2>
						<small>(video & photo galery)</small>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque cursus auctor bibendum. Phasellus tincidunt nisi ut justo scelerisque sodales in eu leo. Praesent id ultrices purus. Vestibulum lacinia eros nec nisl luctus blandit. Ut quis tempus lectus. Quisque scelerisque turpis id nunc congue, sed ullamcorper mauris efficitur.</p>
					</div>
				</div>
			</div>	
		</div>
	</section>

	<section class="team">
		<div class="container">
			<h3 class="text-center agileits agileinfo wthree w3-agileits">Kepengurusan PPBC Pusat</h3>
			<p class="text-center agileits agileinfo wthree w3-agileits">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
					<div class="grid">
						<figure class="effect-julia">
							<img src="<?php echo base_url() ?>assets/ppbc/images/pengurus-1.png" alt="w3layouts" class="img-responsive" title="w3layouts"/>
							<figcaption>
								<div>
									<ul class="social-icons2">	
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>	
								</div>
							</figcaption>			
						</figure>
					</div>
					<h4 class="text-center">Elizabeth</h4>
					<p class="team-p1">Founder</p>	
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
					<div class="grid">
						<figure class="effect-julia">
							<img src="<?php echo base_url() ?>assets/ppbc/images/pengurus-1.png" alt="w3layouts" class="img-responsive" title="w3layouts"/>
							<figcaption>
								<div>
									<ul class="social-icons2">	
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>	
								</div>
							</figcaption>			
						</figure>
					</div>
					<h4 class="text-center">Diana</h4>
					<p class="team-p1">President</p>	
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
					<div class="grid">
						<figure class="effect-julia">
							<img src="<?php echo base_url() ?>assets/ppbc/images/pengurus-1.png" alt="w3layouts" class="img-responsive" title="w3layouts"/>
							<figcaption>
								<div>
									<ul class="social-icons2">	
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>	
								</div>
							</figcaption>			
						</figure>
					</div>
					<h4 class="text-center">Max Payne</h4>
					<p class="team-p1">Secretary</p>	
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
					<div class="grid">
						<figure class="effect-julia">
							<img src="<?php echo base_url() ?>assets/ppbc/images/pengurus-1.png" alt="w3layouts" class="img-responsive" title="w3layouts"/>
							<figcaption>
								<div>
									<ul class="social-icons2">	
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>	
								</div>
							</figcaption>			
						</figure>
					</div>
					<h4 class="text-center">Johnny Blaze</h4>
					<p class="team-p1">Manager</p>				
				</div>
			</div>	
		</div>
	</section>

	<section class="price">
		<div class="container">
			<h3 class="text-center agileits-w3layouts agile w3-agile">Jadilah Bagian Dari Kami</h3>
			<p class="text-center agileits-w3layouts agile w3-agile">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			<div class="cd-pricing-container cd-has-margins text-center">
				<button class="btn-lg">DAFTAR DISINI</button>
			</div> <!-- .cd-pricing-container -->
		</div>
		<!--pop-up-grid -->
		<div id="popup">
			<div id="small-dialog" class="mfp-hide">
				<div class="pop_up">
					<div class="payment-online-form-left">
						<form action="#" method="post">
							<h4><span class="shipping"> </span>Shipping Details</h4>
							<ul>
								<li><input class="text-box-dark" type="text" name="name" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}"></li>
								<li><input class="text-box-dark" type="text" name="name" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}"></li>
							</ul>
							<ul>
								<li><input class="text-box-dark" type="text" name="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}"></li>
								<li><input class="text-box-dark" type="text" name="phone" value="Phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}"></li>
							</ul>
							<ul>
								<li><input class="text-box-dark" type="text" name="address" value="Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Address';}"></li>
								<li><input class="text-box-dark" type="text" name="pincode" value="Pin Code" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Pin Code';}"></li>
							</ul>
							<div class="clear-fix"></div>
							<h4 class="paymenthead"><span class="payment"></span>Payment Details</h4>
							<div class="clear-fix"></div>
							<ul class="payment-type">
								<li><span class="col_checkbox">
									<input id="3" class="css-checkbox1" type="checkbox">
									<label for="3" class="css-label1"></label>
									<a class="visa" href="#"></a>
								</span>												
							</li>
							<li>
								<span class="col_checkbox">
									<input id="4" class="css-checkbox2" type="checkbox">
									<label for="4" class="css-label2"></label>
									<a class="paypal" href="#"></a>
								</span>
							</li>
						</ul>
						<ul>
							<li><input class="text-box-dark" type="text" name="number" value="Card Number" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Card Number';}"></li>
							<li><input class="text-box-dark" type="text" name="name" value="Name on card" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name on card';}"></li>
						</ul>
						<ul>
							<li><input class="text-box-light hasDatepicker" name="date" type="text" id="datepicker" value="Expiration Date" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Expiration Date';}"><em class="pay-date"> </em></li>
							<li><input class="text-box-dark" type="text" name="number" value="Security Code" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Security Code';}"></li>
						</ul>
						<ul class="payment-sendbtns">
							<li><input type="reset" value="Reset"></li>
							<li><input type="submit" value="Submit"></li>
						</ul>
						<div class="clear-fix"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>

	<section class="subs">
		<div class="container">
			<!-- <h3>Subcribe to Our news letter to latest news and offers</h3> -->
			
			<div class="col-lg-6 col-md-6 col-sm-12 text-center">
				<img src="<?php echo base_url() ?>assets/ppbc/images/logos/beacukai-for-footer.png" alt="">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 text-center">
				<img src="<?php echo base_url() ?>assets/ppbc/images/logos/ppbc-for-footer.png" alt="">
			</div>
			<div class="clearfix"></div>
			
		</div>
	</section>

	<?php 
	  $this->load->view('frontend/footer');
	?>
<!-- /footer section -->
<!-- back to top -->
<a href="#0" class="cd-top">Top</a>
<!-- /back to top -->
<!-- js files -->
<script src="<?php echo base_url() ?>assets/ppbc/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/SmoothScroll.min.js"></script>

<!-- scroll to top -->
<script>
	$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $("section.footer a[href='#myPage']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
   if (this.hash !== "") {

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
    	scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
    } // End if
  });
})
</script>
<!-- /scroll to top -->
<script src="<?php echo base_url() ?>assets/ppbc/js/pricetable.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/wmBox.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/info.js"></script>
<!-- js for pricing table pop up -->
<script src="<?php echo base_url() ?>assets/ppbc/js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

	});
</script>
<!-- /js for pricing table pop up -->
<!-- js for back to top -->
<script src="<?php echo base_url() ?>assets/ppbc/js/top.js"></script>
<!-- /js for back to top -->
<!-- js for search button -->
<script src="<?php echo base_url() ?>assets/ppbc/js/classie.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/uisearch.js"></script>
<script>
	// new UISearch( document.getElementById( 'sb-search' ) );
</script>
<!-- /js for search button -->
<script src="<?php echo base_url() ?>assets/ppbc/js/jquery.pogo-slider.min.js"></script>
<script src="<?php echo base_url() ?>assets/ppbc/js/main.js"></script>
<!-- /js files -->
</body>
</html>