<div class="main-content">
	<div class="container">
		<h2 class="title-page">Available Trips</h2>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<div class="row products">
			<?php
			if ($trips->num_rows > 0) {
				foreach ($trips->result() as $trip) {
					if($trip->prime_image) {
						$image = $trip->prime_image;
					} else {
						$image = 'default.jpg';
					}
					?>
					<div class="col-md-4">
						<div class="prod-wrap">
							<div class="img-box"><img src="<?php echo base_url()?>/assets/images/products/<?php echo $image; ?>" alt="<?php echo $trip->name; ?>" /></div>
							<h2><?php echo $trip->name; ?></h2>
							<!-- <h3><i class="fa fa-clock-o"></i>17 Oktober - 23 November</h3> -->
							<p>
								<?php echo substr(strip_tags($trip->description), 0, 150) ?>...
							</p>
							<a href="<?php echo site_url('product/detail/'.$trip->slug) ?>">Selengkapnya <i class="fa fa-mail-forward"></i></a>
						</div>
					</div>
					<?php
					}
				}
				?>
				<div class="col-md-4">
					<div class="prod-wrap">
						<div class="img-box"><img src="<?php echo base_url() ?>assets/images/prod1.jpg" /></div>
						<h2>Custom Trip</h2>
						<!-- <h3><i class="fa fa-clock-o"></i>17 Oktober - 23 November</h3> -->
						<p>
							Tentukan sendiri destinasi dan tipe liburan seperti apa yang Anda inginkan.
						</p>
						<a href="<?php echo site_url('product/custome_request') ?>">Click untuk request...<i class="fa fa-mail-forward"></i></a>
					</div>
				</div>
		</div>
	</div>
</div>