<div class="main-content">
	<div class="container">
		<h2 class="title-page bc">Trip Detail</h2>
		<h3 class="subtitle"><?php echo $detail->name;?></h3>
	</div>
</div>
<div class="lightgrey-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-10 left-content">
				<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
					<?php
					if($images->num_rows > 0) {
						foreach ($images->result() as $image) {
							?>
							<img src="<?php echo base_url() ?>assets/images/products/<?php echo $image->image ?>" data-thumb="<?php echo base_url() ?>assets/images/products/<?php echo $image->image ?>" title="#htmlcaption<?php echo $image->id;?>" alt=""/>
							<?php
						}
					}
					?>
					</div>
					<?php
					if($images->num_rows > 0) {
						foreach ($images->result() as $image) {
							?>
							<div id="htmlcaption<?php echo $image->id ?>" class="nivo-html-caption">
								<p><?php echo $image->description; ?></p>
							</div>
							<?php
						}
					}
					?>    
				</div>
				<div class="prod-desc">
					<h4 class="date"><i class="fa fa-calendar-o"></i><?php echo $detail->duration;?> Days</h4>
					<div class="highlight">
						<?php echo $detail->highlight; ?>
					</div>
					<?php echo $detail->description; ?>
					<a href="<?php echo site_url('index/book/'.$detail->id.'') ?>" class="big-btn"><i class="fa fa-shopping-cart"></i> book now</a>
				</div>
			</div>
			<?php
			if($banners->num_rows > 0) {
				?>			
				<div class="col-md-2 banner">
					<?php
					foreach ($banners->result() as $banner) {
						?>
						<a href="<?php echo $banner->url; ?>"><img src="<?php echo base_url() ?>assets/images/banner/<?php echo $banner->image ?>"></a>
						<?php
					}
					?>
				</div>	
				<?php
			}
			?>
		</div>
	</div>
</div>