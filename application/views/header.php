<?php
/*
 * Declare variable from session
*/
// echo '<pre>';
// print_r($this->session->userdata);
// echo '</pre>';die();
$user_id = $this->session->userdata('UserIDSession');
$firstname = $this->session->userdata('FirstNameSession');
$lastname = $this->session->userdata('LastNameSession');
$username = $this->session->userdata('UserNameSession');
$photo = $this->session->userdata('PhotoSession');
$position = $this->session->userdata('PositionSession');
$email = $this->session->userdata('EmailSession');
$created_at = date("j M Y", strtotime($this->session->userdata('CreatedAtSession')));
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PPBC | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
        <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url() ?>assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url() ?>assets/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo base_url() ?>assets/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="<?php echo base_url() ?>assets/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo base_url() ?>assets/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />


        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo base_url() ?>assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="<?php echo base_url() ?>assets/js/raphael-min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.format.1.05.js"></script>
<!--        <script src="js/plugins/morris/morris.min.js" type="text/javascript"></script>-->
        <!-- Sparkline -->
        <script src="<?php echo base_url() ?>assets/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url() ?>assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="<?php echo base_url() ?>assets/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url() ?>assets/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="<?php echo base_url() ?>assets/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url() ?>assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() ?>assets/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="<?php echo base_url() ?>assets/js/AdminLTE/app.js" type="text/javascript"></script>

        <script src="<?php echo base_url() ?>assets/js/jquery.ajaxupload.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.format.1.05.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <img src="<?php echo base_url()?>assets/images/logon.png">
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $firstname.' '.$lastname; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo base_url(); ?>assets/images/avatar3.png" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $firstname.' '.$lastname. ' - '.$position; ?>
                                        <small>Member since - <?php echo $created_at ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo site_url('profile') ?>" class="btn btn-primary btn-flat"><i class="fa fa-user"></i> Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('user/logout') ?>" class="btn btn-primary btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url() ?>assets/images/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $firstname ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="<?php echo site_url('admin');?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user-md"></i>
                                <span>Management User</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url('user/add'); ?>"><i class="fa fa-angle-double-right"></i> Add New User</a></li>
                                <li><a href="<?php echo site_url('user'); ?>"><i class="fa fa-angle-double-right"></i> List All User</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user-md"></i>
                                <span>Management Anggota</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url('user/add'); ?>"><i class="fa fa-angle-double-right"></i> Add New Anggota</a></li>
                                <li><a href="<?php echo site_url('user'); ?>"><i class="fa fa-angle-double-right"></i> List All Anggota</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Management Page</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url('banner/banner?params=about'); ?>"><i class="fa fa-angle-double-right"></i> Tentang Kami</a></li>
                                <li><a href="<?php echo site_url('banner/banner?params=kegiatan'); ?>"><i class="fa fa-angle-double-right"></i> Kegiatan</a></li>
                                <li><a href="<?php echo site_url('banner/banner?params=agenda'); ?>"><i class="fa fa-angle-double-right"></i> Agenda</a></li>
                                <li><a href="<?php echo site_url('berita/berita?params=berita'); ?>"><i class="fa fa-angle-double-right"></i> Berita</a></li>
                                <li><a href="<?php echo site_url('berita/berita?params=beritaduka'); ?>"><i class="fa fa-angle-double-right"></i> Berita Duka</a></li>
                                <li><a href="<?php echo site_url('berita/berita?params=beritakesehatan'); ?>"><i class="fa fa-angle-double-right"></i> Berita Kesehatan Anggota</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="<?php echo site_url('banner'); ?>">
                                <i class="fa fa-picture-o"></i>
                                <span>Setting Media</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url('banner'); ?>"><i class="fa fa-angle-double-right"></i>Homepage</a></li>
                                <li><a href="<?php echo site_url('banner/slider'); ?>"><i class="fa fa-angle-double-right"></i> Slider</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>