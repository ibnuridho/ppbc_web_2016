<?php
if ($action == 'edit') {
    $name = $product->name;
    $description = $product->description;
    $highlight = $product->highlight;
    $meta_description = $product->meta_description;
    $meta_keyword = $product->meta_keyword;
    $price = $product->price;
    $duration = $product->duration;
    $created_at = $product->created_at;
    $status = $product->status;
    $id = $product->id;
} else {
    $name = '';
    $description = '';
    $highlight = '';
    $meta_description = '';
    $meta_keyword = '';
    $price = 0;
    $duration = '';
    $created_at = '';
    $id = '';
    $status = '';
}
?>
<!--Load TinyMCE-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.edithtml",
        plugins: [
            "advlist autolink lists link preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime table contextmenu paste"
        ],
        menubar:false,
        tools: "inserttable"
    });
</script>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <div class="content">
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#general-info" data-toggle="tab"><h4> <i class="fa fa-plus-square-o"></i> General Info</h4></a></li>
                    <li><a href="#product-images" <?php if ($action == 'edit') echo 'data-toggle="tab"' ?> id="tab-images"><h4> <i class="fa fa-plus-square-o"></i> Images</h4></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="general-info">
                        <div class="box-body row">
                            <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="product_error" style="display: none;">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                            </div>
                            <form id="product_form" action="<?php echo $action_form ?>" class="add-margin-left">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Trip Title</label>
                                        <div class="input-group">
                                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                                            <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                                            <input class="form-control" name="name" id="name" type="text" value="<?php echo $name; ?>" placeholder="Trip Title ...">
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div>
<!--                                     <div class="form-group">
                                        <label>Additional Price</label>
                                        <div class="input-group">
                                            <input class="form-control" name="price" id="price" type="text" value="<?php echo $price; ?>"  placeholder="Additional Price ...">
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <label>Trip Duration</label>
                                        <div class="input-group">
                                            <input class="form-control" name= "duration" id="duration" type="text" value="<?php echo $duration; ?>"  placeholder="Trip Duration ...">
                                            <span class="input-group-addon">Day</span>
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <div class="input-group">
                                            <textarea name="meta_description" rows="3" cols="89" class="" id="meta_description" placeholder="Meta Description ..."><?php echo $meta_description ?></textarea>
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Keyword</label>
                                        <div class="input-group">
                                            <textarea name="meta_keyword" rows="3" cols="89" class="" id="meta_keyword" placeholder="Meta Keyword ..."><?php echo $meta_keyword ?></textarea>
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <div class="input-group">
                                            <textarea name="description" rows="3" cols="77" class="edithtml" id="description" placeholder="Meta Description ..."> <?php echo $description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Highlight</label>
                                        <div class="input-group">
                                            <textarea name="highlight" class="edithtml" id="highlight" placeholder="Highligth ..."> <?php echo $highlight; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label></label>
                                        <button class="btn bg-orange btn-flat" type="submit"><i class="fa fa-save"></i> Save</button>&nbsp;&nbsp;<button class="btn bg-orange margin btn-flat"><i class="fa fa-undo"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.tab-pane -->
                    </div>
                    <div class="tab-pane" id="product-images">
                        <div class="box-body row">
                            <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="image_error" style="display: none;">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                            </div>
                            <div class="alert alert-success alert-dismissable col-xs-7 add-margin-left" id="image_success" style="display: none;">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b><i class="fa fa-check-circle-o"></i> Success!</b> <p id="alert_message"></p>
                            </div>
                            <div class="box-body table-responsive col-xs-12">
                                <div class="row">
                                    <div class="col-xs-5 col-md-3">
                                        <a href="javascript:void(0)" class="thumbnail" onclick="">
                                            <img src="<?php echo base_url(); ?>assets/images/upload.png" id="img-uploader" alt="...">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-12">
                                <div class="row" id="list-image">
                                    <?php
                                    if ($action == 'edit') {
                                        if ($images->num_rows > 0) {
                                            foreach ($images->result() as $image) {
                                                ?>
                                                <div class="col-xs-5 col-md-3" id="image-<?php echo $image->id ?>">
                                                    <div class="hover-item">
                                                        <a class="thumbnail">
                                                            <img src="<?php echo base_url(); ?>assets/images/products/<?php echo $image->image ?>">
                                                        </a>
                                                        <div class="overlay">
                                                            <a class="preview btn btn-danger" title="Update Description Image" href="javascript:void(0)" onclick="ShowModalImageDescription(<?php echo $image->id ?>)"><i class="fa fa-plus"></i></a>
                                                            <a class="preview btn btn-danger" title="Delete Image" href="javascript:void(0)" onclick="DeleteImage(<?php echo $image->id ?>)"><i class="fa fa-eraser"></i></a>
                                                            <a class="preview btn btn-danger" title="Set As Prime Image" href="javascript:void(0)" onclick="SetPrimeImage(<?php echo $image->id ?>)"><i class="fa fa-wrench"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div><!-- /.col -->
</div>
</div>
</aside>
<div class="modal fade" id="DescriptionImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Add Description</h4>
            </div>
            <div class="modal-body">
                <div class="box-body row">
                    <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="master_bank_error" style="display: none;">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                    </div>
                    <form id="DescriptionImageForm" action="<?php echo site_url('product/AddImageDescription') ?>" class="">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <div class="input-group">
                                    <input type="hidden" name="image_id" id="image_id">
                                    <textarea name="description_image" class="" id="description_image" placeholder="Description Image ..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-yes" ><i class="fa fa-save"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
            </div>
            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">

//function untuk upload image, parsing product_id(mandatory)
    $(function() {
        var product_id = $('#id').val();
        if ($.isNumeric(product_id)) {
            image = $('#image');
            var uploadLink = $('#img-uploader');
            var list_image = $('#list-image');
            new AjaxUpload(uploadLink, {
                action: '<?php echo site_url('upload/ImageProduct') ?>',
                data: {'product_id': product_id},
                name: 'image',
                onSubmit: function(file, ext) {
                    if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                        $('.loader-page').fadeOut();
                        $('#image_error').show();
                        $('#image_error #alert_message').html('Only JPG, PNG or GIF files are allowed');
                        return false;
                    }
                }, onComplete: function(file, response) {
                    //On completion clear the status
                    var json = $.parseJSON(response);
                    //Add uploaded file to list
                    if (json.result === "success") {
                        list_image.append('<div class="col-xs-5 col-md-3" id="image-' + json.image_id + '">\n\
                                              <div class="hover-item">\n\
                                                   <a class="thumbnail"><img src="<?php echo base_url(); ?>assets/images/products/' + json.filename + '"></a>\n\
                                                   <div class="overlay"><a class="preview btn btn-danger" title="Add Description Image" href="javascript:void(0)" onclick="ShowModalImageDescription(' + json.image_id + ')"><i class="fa fa-plus"></i></a><a class="preview btn btn-danger" title="Delete Image" href="javascript:void(0)" onclick="DeleteImage(' + json.image_id + ')"><i class="fa fa-eraser"></i></a><a class="preview btn btn-danger" title="Set As Prime Image" href="javascript:void(0)" onclick="SetPrimeImage(' + json.image_id + ')"><i class="fa fa-wrench"></i></a></div>\n\
                                               </div>\n\
                                            </div>');
                    } else {
                        $('#image_error').show();
                        $('#image_error #alert_message').html('Failed to upload images.');
                    }
                }
            });
        } else {
            $('#image_error').show();
            $('#image_error #alert_message').html('Please fill product form first.');
        }
    });

    //delete image by id
    function DeleteImage(id) {
        $('#ConfirmDelete').modal('show');
        $('#yes-delete').click(function() {
            $('.loader-page').fadeIn();
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('product/DeleteImage') ?>',
                data: 'id=' + id,
                dataType: 'json',
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        $('#ConfirmDelete').modal('hide');
                        $('#image-' + id).remove();
                    } else {
                        alert("Opration Failed !");
                    }
                }
            })
            return false;
        });
    }

    //delete image by id
    function SetPrimeImage(id) {
        $('.loader-page').fadeIn();
        var product_id = $('#id').attr('value');
        if (product_id !== '' || product_id !== 0) {
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('product/SetPrimeImage') ?>',
                data: 'id=' + id + '&product_id=' + product_id,
                dataType: 'json',
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        $('#InformModal').modal('show');
                        $('#InformModal .modal-body').html(data.message);
                    } else {
                        alert('Opration Failed !');
                    }
                }
            })
            return false;
        } else {
            alert('Opration Failed !');
        }
    }

//  function untuk menambahkan description pada image
    function ShowModalImageDescription(image_id) {
        $('#DescriptionImageModal').modal('show');
        if (image_id !== '' || image_id !== 0) {
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('product/GetDescriptionImage') ?>',
                data: 'id=' + image_id,
                dataType: 'json',
                success: function(data) {
                    $('#image_id').val(image_id);
                    $('#description_image').val(data.description);
                }
            });
            return false;
        } else {
            alert('Opration Failed !');
        }
    }
    $(document).ready(function() {
        //set input price numurice use jquery format
        $('#price').format();
        $('#duration').format();
        $('#maximum_buy').format();
        $('#minimum_buy').format();

        //set function untuk show/hide property pisical
        $('#product_form').submit(function() {
//            $('.loader-page').fadeIn();
            tinyMCE.triggerSave();

            //get value from tinyMCE
            content = tinyMCE.get('description');
            description = content.getContent();
            content = tinyMCE.get('highlight');
            highlight = content.getContent();

            var name = $('#name').val();
            meta_description = $('#meta_description').val();
            meta_keyword = $('#meta_keyword').val();
            price = $('#price').val();
            duration = $('#duration').val();
            maximum_buy = $('#maximum_buy').val();
            minimum_buy = $('#minimum_buy').val();
            id = $('#id').val();
//            status = $('#status').val();
            action = $('#action').val();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                dataType: 'json',
                data: 'action=' + action + '&name=' + name + '&meta_description=' + meta_description + '&meta_keyword=' + meta_keyword + '&price=' + price +
                        '&duration=' + duration + '&maximum_buy=' + maximum_buy + '&minimum_buy=' + minimum_buy + '&id=' + id + '&description=' + description + '&highlight=' + highlight,
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        $('#id').val(data.id);
                        $('#InformModal').modal('show');
                        $('#product_error').hide();
                        $('#InformModal .modal-body').html(data.message);
                        $('#tab-images').attr('data-toggle', 'tab');
                        $('#btn-ok').click(function() {
                            $('#InformModal').modal('hide');
                        });
                    } else {
                        $('#product_error').show();
                        $('#product_error #alert_message').html(data.message);
                    }
                }
            });
            return false;
        });
        

         $('#DescriptionImageForm').submit(function() {
//            $('.loader-page').fadeIn();
            //get value from tinyMCE
//            content = tinyMCE.get('description_image');
            description_image = $('#description_image').val();
            image_id = $('#image_id').val();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                dataType: 'json',
                data: 'image_id='+image_id+'&description_image='+description_image,
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        $('#id').val(data.id);
                        $('#DescriptionImageModal').modal('hide');
                        alert(data.message)
                    } else {
                        alert('Gagal Update description image.');
                    }
                }
            });
            return false;
        });
    });
</script>