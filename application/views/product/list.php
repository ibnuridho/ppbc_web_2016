<aside class="right-side">
    <section class="content-header">
        <h1>
            <i class="fa fa-list-alt"></i> List All Trip
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('product'); ?>">Product List</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th>Trip Name</th>
                                    <th>Highlight</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($products->num_rows > 0) {
                                    foreach ($products->result() as $product) {
                                        if ($product->status == 1) {
                                            $status = '<label class="label label-info">Active</label>';
                                        } else {
                                            $status = '<label class="label label-warning">Deactive</label>';
                                        }
                                        ?>
                                        <tr class="user-row-<?php echo $product->id ?>">
                                            <td><?php echo $product->name; ?></td>
                                            <td><?php echo substr($product->highlight, 0,100) ?></td>
                                            <td>
                                            <a href="<?php echo site_url('product/edit') . '/' . $product->id ?>"><button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button></a>
                                            <a href="javascript:void(0)" onclick="confirm_delete(<?php echo $product->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<script type="text/javascript">
        function confirm_delete(id) {
            $('#ConfirmDelete').modal('show');
            $('#yes-delete').click(function(){
                $('.loader-page').fadeIn();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('product/delete') ?>',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data) {
                        $('.loader-page').fadeOut();
                        if (data.error === 0) {
                            $('#ConfirmDelete').modal('hide');
                            $('.user-row-'+id).remove();
                        } else {
                            alert("Opration Failed !");
                        }
                    }
                })
                return false;
            });
        }
</script>