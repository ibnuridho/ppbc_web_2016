<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-edit"></i> Add New Product
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('product'); ?>">Management Product</a></li>
        </ol>
    </section>
    <div class="row">
        <div class="box-header">
            <a href="<?php echo site_url('product') ?>" class="btn btn-app btn-back btn-position-left">
                <i class="fa fa-repeat"></i> Back
            </a>
        </div><!-- /.box-header -->
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active" id="list_info"><a href="#product_info" data-toggle="tab"><i class="fa fa-info"></i> Product Information </a></li>
                    <li id="list_detail"><a href="#product_details" data-toggle="tab"><i class="fa fa-book"></i> More Product Details </a></li>
                    <li id="list_options"><a href="#product_selling" data-toggle="tab"><i class="fa fa-money"></i> Selling Option </a></li>
                    <li  id="list_images"><a href="#product_images" data-toggle="tab"><i class="fa fa-picture-o"></i> Product Images </a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="product_info">
                        <div class="box-body row">
                            <div class="box-body col-md-6">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <div class="input-group">
                                        <input class="form-control" name="name" id="name" type="text" value="" placeholder="Product Name ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Model</label>
                                    <div class="input-group">
                                        <input class="form-control" name="model" id="model" type="text" value=""  placeholder="Model ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Choose Type Product</label>
                                    <div class="input-group">
                                        <select name="product_type_id" id="product_type_id" class="form-control">
                                            <option value=""> - Choose Product Type - </option>
                                            <option value="1"> Physical </option>
                                            <option value="2"> Services </option>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>SKU</label>
                                    <div class="input-group">
                                        <input class="form-control" name="sku" id="sku" type="text" value="" placeholder="Stock Keeping Unit ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>UPC</label>
                                    <div class="input-group">
                                        <input class="form-control" name="upc" id="upc" type="text" value="" placeholder="Universal Product Code ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Meta Description</label>
                                    <div class="input-group">
                                        <textarea name="meta_description" rows="3" cols="69" class="" id="meta_description" placeholder="Meta Description ..."></textarea>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Meta Keyword</label>
                                    <div class="input-group">
                                        <textarea name="meta_description" rows="3" cols="69" class="" id="meta_description" placeholder="Meta Description ..."></textarea>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <div class="input-group">
                                            <textarea name="meta_description" rows="3" cols="69" class="" id="meta_description" placeholder="Meta Description ..."></textarea>
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div>
                                    <label>Category</label>
                                    <div class="input-group">
                                        <select multiple="" name="category_id" id="category_id" class="form-control">
                                            <option>option 1</option>
                                            <option>option 2</option>
                                            <option>option 3</option>
                                            <option>option 4</option>
                                            <option>option 5</option>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Sub Category</label>
                                    <div class="input-group">
                                        <select multiple="" name="category_id" id="category_id" class="form-control">
                                            <option>option 1</option>
                                            <option>option 2</option>
                                            <option>option 3</option>
                                            <option>option 4</option>
                                            <option>option 5</option>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group pull-right">
                                    <button type="button" class="btn btn-info" id="btn_next_1" data-dismiss="modal"><i class="fa fa-sign-in"></i> Next</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="product_details">
                        <div class="box-body row">
                            <div class="box-body col-md-6">
                                <div class="form-group">
                                    <label>Manufacture</label>
                                    <div class="input-group">
                                        <select name="manufacture_id" id="manufacture_id" class="form-control">
                                            <option value=""> - Choose Manufacture - </option>
                                            <option value="1"> Adidas </option>
                                            <option value="2"> Nike </option>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Date Available</label>
                                    <div class="input-group">
                                        <input class="form-control" name="date_available" id="date_available" type="text" value="" placeholder="Date Available ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Stock Status</label>
                                    <div class="input-group">
                                        <select name="stock_status_id" id="stock_status_id" class="form-control">
                                            <option value=""> - Choose Stock Status - </option>
                                            <option value="5"> Out Of Stocks </option>
                                            <option value="6"> 2 - 3 Days </option>
                                            <option value="7"> In Stock </option>
                                            <option value="8"> Pre Order </option>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">.00</span>
                                        <input class="form-control" name="price" id="price" type="text" value="" placeholder="Price...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Quantity to Sale</label>
                                    <div class="input-group">
                                        <input class="form-control" name="quantity_to_sale" id="quantity_to_sale" type="text" value="" placeholder="Product Name ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div><div class="form-group">
                                    <label>Minimum Buy</label>
                                    <div class="input-group">
                                        <input class="form-control" name="minimum_buy" id="minimum_buy" type="text" value="" placeholder="Minimum Buy ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Maximum Buy</label>
                                    <div class="input-group">
                                        <input class="form-control" name="maximum_buy" id="maximum_buy" type="text" value="" placeholder="Maximum Buy ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Points</label>
                                    <div class="input-group">
                                        <input class="form-control" name="points" id="points" type="text" value="" placeholder="Points ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Handling Fee</label>
                                    <div class="input-group">
                                        <input class="form-control" name="handling_fee" id="handling_fee" type="text" value="" placeholder="Handling Fee ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Sort Order</label>
                                    <div class="input-group">
                                        <input class="form-control" name="sort_order" id="sort_order" type="text" value="" placeholder="Sort Order ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body table-responsive col-md-6" id="physical_property">
                                <div class="form-group" style="padding-bottom: 13px;">
                                    <label>Requires Shipping</label>
                                    <div class="input-group">
                                        <input class="form-control" name="shipping" id="shipping" value="1" type="radio"> Yes <input class="form-control" name="shipping" id="shipping" value="0" type="radio"> No
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Shipping Option</label>
                                    <div class="input-group">
                                        <select name="manufacture_id" id="manufacture_id" class="form-control">
                                            <option value=""> - Choose Shipping Option - </option>
                                            <option value="0"> Default Shipping Option </option>
                                            <option value="2"> Free Shipping </option>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Location</label>
                                    <div class="input-group">
                                        <input class="form-control" name="location" id="location" type="text" value="" placeholder="Location ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Dimensions</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">L</span>
                                        <input class="col-lg-5" name="length" id="length" type="text" value="">
                                        <span class="input-group-addon">W</span>
                                        <input class="col-md-5" name="width" id="width" type="text" value="">
                                        <span class="input-group-addon">H</span>
                                        <input class="col-md-5" name="height" id="height" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Weight</label>
                                    <div class="input-group">
                                        <input class="form-control" name="weight" id="weight" type="text" value="" placeholder="Weight ...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <a href="#product_info" type="button" class="btn btn-info" id="btn_back_2" data-dismiss="modal"><i class="fa fa-mail-reply"></i> Back</a>
                                <a href="#product_pricing" type="button" class="btn btn-info" id="btn_next_2" data-dismiss="modal"><i class="fa fa-sign-in"></i> Next</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="product_selling">
                        <div class="box-body row">
                            <div class="box-body col-md-6">
                                <h3>Selling Options</h3>
                                <div class="form-group">
                                    <label>Options <span class="required">*</span></label>
                                    <div class="input-group">
                                        <select name="option_id" id="option_id" class="form-control">
                                            <option value="">- Choose Options -</option>
                                            <?php
                                            foreach ($options as $option) {
                                                ?>
                                                <option value="<?php echo $option->option_id ?>" type="<?php echo $option->type ?>"><?php echo $option->name ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>

                                <div id="content_option">
                                </div>
                                <!-- set componen untuk masing masing tipe option-->
                                <!--Text Type-->
                                <div id="select" style="display: none;">
                                    <div class="col-md-5">
                                        <div id="text" class="form-group">
                                            <label>Value <span class="required">*</span></label>
                                            <select name="option_id" id="option_id" class="form-control">
                                                <option value="">- Choose Options -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Options <span class="required">*</span></label>
                                        <div class="input-group">
                                            <select name="option_id" id="option_id" class="form-control">
                                                <option value="">- Choose Options -</option>
                                                <?php
                                                foreach ($options as $option) {
                                                    ?>
                                                    <option value="<?php echo $option->option_id ?>" type="<?php echo $option->type ?>"><?php echo $option->name ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="text" class="form-group">
                                            <label>Quantity to Sale <span class="required">*</span></label>
                                            <input type="text">
                                        </div>
                                        <div id="text" class="form-group">
                                            <label>Points <span class="required">*</span></label>
                                            <div class="input-group">
                                                <select >
                                                    <option>+</option>
                                                    <option></option>
                                                </select><input type="text">
                                            </div>
                                        </div>
                                        <div id="text" class="form-group">
                                            <label>Points <span class="required">*</span></label>
                                            <div class="input-group">
                                                <select >
                                                    <option>+</option>
                                                    <option>-</option>
                                                </select><input type="text">
                                            </div>
                                        </div>
                                        <div id="text" class="form-group">
                                            <label>Points <span class="required">*</span></label>
                                            <div class="input-group">
                                                <select >
                                                    <option>+</option>
                                                    <option></option>
                                                </select><input type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Text Type-->
                                <div id="text" class="form-group"  style="display: none;">
                                    <label>Text <span class="required">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">T</span>
                                        <input class="form-control" name="location" id="location" type="text" value="" placeholder="Input text here...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>

                                <!--Date Type-->
                                <div id="date" class="form-group" style="display: none;">
                                    <label>Date <span class="required">*</span></label>
                                    <div class="input-group" id="">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="date" name="po_date" id="po_date" class="form-control" />
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>

                                <!--Date Type-->
                                <div id="datetime" class="form-group" style="display: none;">
                                    <label>Date <span class="required">*</span></label>
                                    <div class="input-group" id="">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="datetime" name="po_time" id="po_time" class="form-control" />
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>
                                <!--Time Type-->
                                <div id="text" class="form-group"  style="display: none;">
                                    <label>Time <span class="required">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        <input class="form-control timepicker" name="po_time" id="po_time" type="text" value="" placeholder="Input text here...">
                                        <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                    </div>
                                </div>

                                <!--Trigger button-->
                                <div class="form-group">
                                    <a class="btn btn-info"><i class="fa fa-plus-square-o"></i> Add Option</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="product_images">
                        <div class="box-body row">
                            <div class="box-body table-responsive col-xs-12">
                                <div class="row">
                                    <div class="col-xs-5 col-md-3">
                                        <a href="#" class="thumbnail">
                                            <img src="<?php echo base_url(); ?>assets/images/add.jpg" alt="...">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->
        </div><!-- /.col -->
    </div> <!-- /.row -->
</aside>
<script type="text/javascript">
    $(document).ready(function() {
        //set function untuk show/hide property pisical
        $('#btn_back_2').click(function() {
            $('#list_info').addClass('active');
            $('#list_detail').removeClass('active');
        });
        $("#product_type_id").change(function() {
            var type = $('#product_type_id').val();
            if (type === '2') {
                $('#physical_property').hide();
            } else if (type === '1') {
                $('#physical_property').show();
            }
        });
        $(".timepicker").timepicker({
            showInputs: true
        });
        $("#option_id").change(function() {
            var type = $('option:selected', this).attr('type');
            var content = $('#' + type).clone().removeAttr('style');
            $('#content_option').html(content);
        });
    });
</script>