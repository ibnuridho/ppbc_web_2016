<!DOCTYPE html
  PUBLIC"-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
  </head>
  <body>
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="padding: 20px; background: #e5e5e5;">
					<table cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr>
								<td style="height: 72px; background: #FFF; height:71px; width:650px; border: solid 1px #bbb; border-bottom:none; border-radius:5px 5px 0 0;">
									<img src="http://PPBC.org/assets/frontend/images/logo.png" style="margin-left:20px;width: 150px;" alt="Manatrip" />
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="background-color:#F4F4F4; padding: 15px; margin:0; border:solid 1px #bbb; border-top:none; padding-top:0;">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td style="width:618px; background:#FFF; border: solid 1px #e5e5e5; padding:20px; border-radius: 5px; -webkit-border-radius:5px; -moz-border-radius:5px">
												<h2 style="font-family:'arial'; font-size:18px; font-weight: normal; color:#696969; margin:0;">Hi Support..</h2>
												<p style="font-family:'arial'; font-size:12px; color:#696969; margin:0;">Anda menerima satu request baru :</p>
												<br/>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Type  : <strong><?php echo $type; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Budget  : <strong><?php echo number_format($budget); ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Teman Liburan &nbsp;: <strong><?php echo $friend; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Destinasi &nbsp;: <strong><?php echo $destination; ?></strong></p>
												<?php
												if($member) {
												?>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Jumlah Peserta &nbsp;: <strong><?php echo $member; ?></strong></p>
												<?php
												}
												?>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Tanggal Pemberangkatan &nbsp;: <strong><?php echo $date; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Lama Liburan &nbsp;: <strong><?php echo $duration; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Nama Pengirim &nbsp;: <strong><?php echo $name; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Alamat Pengirim &nbsp;: <strong><?php echo $address; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Email Pengirim &nbsp;: <strong><?php echo $email; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Telepon Pengirim &nbsp;: <strong><?php echo $phone; ?></strong></p>
												<p style="margin:0; font-family:'arial'; font-size:12px; color:#696969;">Message &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $comment; ?></p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
  </body>
</html>