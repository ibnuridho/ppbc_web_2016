<aside class="right-side">
    <section class="content-header">
        <h1>
            <a href="javascript:void(0)" onclick="AddNew()"><i class="fa fa-plus-square"></i> Add New</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('pages'); ?>">pages Management</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div id="dataTable">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="" class="table table-bordered table-striped is-datatable">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Menu Title</th>
                                    <th>Content</th>
                                    <th>Image</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($pagess->num_rows > 0) {
                                    foreach ($pagess->result() as $pages) {
                                        if ($pages->status == 1) {
                                            $status = '<label class="label label-info">Active</label>';
                                        } else {
                                            $status = '<label class="label label-warning">Deactive</label>';
                                        }
                                        ?>
                                        <tr class="pages-row-<?php echo $pages->id ?>">
                                            <td><?php echo $pages->code; ?></td>
                                            <td><?php echo $pages->name; ?></td>
                                            <td><?php echo $pages->url; ?></td>
                                            <td>
                                                <a href="javascript:void(0)" class="thumbnail" onclick="UploadImage()">
                                                    <img src="<?php echo base_url(); ?>assets/images/pages/<?php echo $pages->image; ?>" class="pages-thumbnail" alt="...">
                                                </a>
                                            </td>
                                            <td><?php echo $pages->position ?></td>
                                            <td><?php echo $status ?></td>
                                            <td><a href="javascript:void(0)" onclick="confirm_delete(<?php echo $pages->id; ?>)"><button class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i></button></a> <a href="javascript:void(0)" onclick="Edit(<?php echo $pages->id ?>)"><button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button></a></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</aside>

<!-- Modal Confirmasi Delete -->
<div class="modal fade" id="pagesForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Add New</h4>
            </div>
            <div class="modal-body">
                <div class="box-body row">
                    <div class="alert alert-danger alert-dismissable col-xs-7 add-margin-left" id="pages_error" style="display: none;">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                    </div>
                    <form id="pages_form" action="<?php echo $action_form ?>" class="">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>pages Code</label>
                                <div class="input-group">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                                    <input class="form-control" name="code" id="code" type="text" value="" placeholder="Code pages ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>pages Name</label>
                                <div class="input-group">
                                    <input class="form-control" name="name" id="name" type="text" value=""  placeholder="pages Name ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>URL</label>
                                <div class="input-group">
                                    <input class="form-control timepicker" name= "url" id="url" type="text" value=""  placeholder="URL ...">
                                    <span class="input-group-addon"><a href="#" data-toggle="tooltip" title="Some tooltip text!"><i class="fa fa-info-circle"></i> </a></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>pages Position</label>
                                <select name="position" id="position" class="form-control">
                                    <option value="">- Choose Position - </option>
                                    <option value="package_detail"> Package Detail </option>
                                    <option value="static_pages"> Static pages </option>
                                    <option value="trip_detail"> Trip Detail </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">- Choose State - </option>
                                    <option value="1"> Active </option>
                                    <option value="0"> Deactive </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>pages Image</label>
                                <div class="input-group col-md-5">
                                    <input type="hidden" name="image" id="image">
                                    <a href="javascript:void(0)" class="thumbnail" onclick="UploadImage()">
                                        <img src="<?php echo base_url(); ?>assets/images/upload.png" id="img-uploader" alt="...">
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-yes btn-flat" id=""><i class="fa fa-save"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
            </div>
            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
                function confirm_delete(id) {
                    $('#ConfirmDelete').modal('show');
                    $('#yes-delete').click(function() {
                        $('.loader-pages').fadeIn();
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo site_url('pages/delete') ?>',
                            data: 'id=' + id,
                            dataType: 'json',
                            success: function(data) {
                                $('.loader-pages').fadeOut();
                                if (data.error === 0) {
                                    $('#ConfirmDelete').modal('hide');
                                    $('.pages-row-' + id).remove();
                                } else {
                                    alert("Opration Failed !");
                                }
                            }
                        })
                        return false;
                    });
                }

                function AddNew() {
                    clearForm('pages_form');
                    hideAlert();
                    $('#pagesForm').modal('show');
                }

                function Edit(id) {
                    if (id) {
                        $('.loader-pages').fadeIn();
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo site_url('pages/GetpagesByID') ?>',
                            dataType: 'json',
                            data: 'id=' + id,
                            success: function(data) {
                                $('.loader-pages').fadeOut();
                                if (data.error === 0) {
                                    $('#pagesForm').modal('show');
                                    $('#code').val(data.code);
                                    $('#name').val(data.name);
                                    $('#url').val(data.url);
                                    $('#id').val(data.id);
                                    $('#position').val(data.position);
                                    $('#status').val(data.status);
                                    $('#image').val(data.image);
                                    $('#action').val(data.action);
                                    $('#img-uploader').attr('src', '<?php echo base_url() ?>assets/images/pages/' + data.image);
                                } else {
                                    $('#pages_error').show();
                                    $('#pages_error #alert_message').html(data.message);
                                }
                            }
                        });
                        return false;
                    }
                }

                function UploadImage() {
                    image = $('#image');
                    var uploadLink = $('#img-uploader');
                    var list_image = $('#list-image');
                    new AjaxUpload(uploadLink, {
                        action: '<?php echo site_url('upload/pages') ?>',
                        name: 'image',
                        onSubmit: function(file, ext) {
                            if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                                $('.loader-pages').fadeOut();
                                $('#image_error').show();
                                $('#image_error #alert_message').html('Only JPG, PNG or GIF files are allowed');
                                return false;
                            }
                        }, onComplete: function(file, response) {
                            //On completion clear the status
                            var json = $.parseJSON(response);
                            //Add uploaded file to list
                            if (json.result === "success") {
                                $('#img-uploader').attr('src', '<?php echo base_url() ?>assets/images/pages/' + json.filename);
                                $('#image').val(json.filename);
                            } else {
                                $('#pages_error').show();
                                $('#pages_error #alert_message').html('Failed to upload images.');
                            }
                        }
                    });
                }
                $(document).ready(function() {
                    //form submit untuk form credential
                    $("#pages_form").submit(function() {
                        $('.loader-pages').fadeIn();
                        $.ajax({
                            type: 'POST',
                            url: $(this).attr('action'),
                            dataType: 'json',
                            data: $(this).serialize(),
                            success: function(data) {
                                $('.loader-pages').fadeOut();
                                if (data.error === 0) {
                                    window.location.href = data.redirect;
                                } else {
                                    $('#pages_error').show();
                                    $('#pages_error #alert_message').html(data.message);
                                }
                            }
                        });
                        return false;
                    });
                });
</script>