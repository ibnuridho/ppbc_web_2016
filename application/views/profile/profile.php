<?php
if ($profile->image != '') {
    $photo = $profile->image;
} else {
    $photo = 'no-profile-image.png';
}
?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Profile
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('dashboard/profile'); ?>">Profile</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <!-- Custom Tabs (Pulled to the right) -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_profile_info" data-toggle="tab"><h4> <i class="fa fa-info-circle"></i> Profile Info</h4></a></li>
                    <li><a href="#tab_edit_info" data-toggle="tab"> <h4> <i class="fa fa-edit"></i> Edit Info</h4></a></li>
                    <li><a href="#tab_edit_credential" data-toggle="tab"><h4><i class="fa fa-key"></i> Edit Credential</h4></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_profile_info">
                        <div class="box-body row">
                            <div class="box-body table-responsive col-xs-7">
                                <table class="table table-hover">
                                    <tr>
                                        <td>Username</td>
                                        <td><?php echo $profile->username ?></td>
                                    </tr>
                                    <tr>
                                        <td>Fist Name</td>
                                        <td><?php echo $profile->firstname ?></td>
                                    </tr>
                                    <tr>
                                        <td>Last Name</td>
                                        <td><?php echo $profile->lastname ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $profile->email ?></td>
                                    </tr>
                                    <tr>
                                        <td>Date Member</td>
                                        <td>Since, <?php echo date('j M Y', strtotime($profile->created_at)); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_edit_info">
                        <div class="box-body row">
                            <div class="alert alert-danger alert-dismissable col-xs-7" id="error_credential" style="display: none;">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Alert!</b> <p id="alert_message"></p>
                            </div>
                            <div class="alert alert-danger alert-dismissable col-xs-7" id="error_info" style="display: none;">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                            </div>
                            <form id="form_info" action="<?php echo $action_form_info ?>">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="form-control" name="username" id="username" type="text" value="<?php echo $profile->username ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input class="form-control" name="firstname" id="firstname" type="text" value="<?php echo $profile->firstname ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input class="form-control" name="lastname" id="lastname" type="text" value="<?php echo $profile->lastname ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" name="email" id="email" type="text" value="<?php echo $profile->email ?>">
                                    </div>
                                    <div class="form-group pull-right">
                                        <label></label>
                                        <button class="btn bg-orange margin btn-flat" type="submit">Update Info</button><button class="btn bg-orange margin btn-flat">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_edit_credential">
                        <div class="box-body row">
                            <div class="alert alert-danger alert-dismissable col-xs-7" id="error_credential" style="display: none;">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b><i class="fa fa-exclamation-triangle"></i> Alert!</b> <p id="alert_message"></p>
                            </div>
                            <form id="form_credential" action="<?php echo $action_form_credential ?>">
                                <div class="form-group col-xs-7">
                                    <label>Password</label>
                                    <input class="form-control" name="password" id="password" type="password" placeholder="Password">
                                </div>
                                <div class="form-group col-xs-7">
                                    <label>Confirm Password</label>
                                    <input class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="Confirm Password">
                                </div>
                                <div class="form-group col-xs-7">
                                    <button class="btn bg-orange margin btn-flat" type="submit">Update Credential</button><button class="btn bg-orange margin  btn-flat" id="">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
        </div>
    </section><!-- /.col -->
</aside>
<script type="text/javascript">
    function UploadImage() {
        var uploadLink = $('#uploadLink');
        image = $('#image');
        new AjaxUpload(uploadLink, {
            action: '<?php echo site_url('upload/UploadImage') ?>',
            name: 'image',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                    // extension is not allowed
                    alert('Only JPG, PNG or GIF files are allowed');
                    return false;
                }
            }, onComplete: function(file, response) {
                var json = $.parseJSON(response);
                //Add uploaded file to list
                if (json.result === "success") {
                    uploadLink.html('<img src="<?php echo base_url() ?>assets/img/' + json.filename + '" alt="Profile Photo" style="width : 100px"/> ');
                    image.val(json.filename);
                } else {
                    uploadLink.html('<img src="<?php echo base_url('assets/img/no-profile-image.png') ?>" title="Click to Change Image"  style="width : 100px"/> <br />Failed');
                    image.val(json.filename);
                }
            }
        });
    }
    $(document).ready(function() {
        //get zone by ajax
        $('#country_id').change(function() {
            $('.loader-page').fadeIn();
            var country_id = $(this).val();
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('country/GetSelectBoxZone'); ?>',
                data: 'country_id=' + country_id,
                success: function(data) {
                    $('#zone_id').html(data);
                    $('.loader-page').fadeOut();
                }
            });
            return false;
        });

        //get zone by ajax
        $('#zone_id').change(function() {
            $('.loader-page').fadeIn();
            var zone_id = $(this).val();
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('country/GetSelectBoxCity'); ?>',
                data: 'zone_id=' + zone_id,
                success: function(data) {
                    $('#city_id').html(data);
                    $('.loader-page').fadeOut();
                }
            });
            return false;
        });

        //form submit untuk form credential
        $("#form_info").submit(function() {
            $('.loader-page').fadeIn();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                dataType: 'json',
                data: $(this).serialize(),
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        window.location.href = data.redirect;
                    } else {
                        $('#error_info').show();
                        $('#error_info #alert_message').html(data.message);
                    }
                }
            });
            return false;
        });

        //form submit untuk form credential
        $("#form_credential").submit(function() {
            $('.loader-page').fadeIn();
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                dataType: 'json',
                data: $(this).serialize(),
                success: function(data) {
                    $('.loader-page').fadeOut();
                    if (data.error === 0) {
                        window.location.href = data.redirect;
                    } else {
                        $('#error_credential').show();
                        $('#error_credential #alert_message').html(data.message);
                    }
                }
            });
            return false;
        });
    });
</script>
