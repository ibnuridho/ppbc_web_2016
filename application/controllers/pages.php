<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('PageModel','SliderModel'));
    }

    function Index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'pages/about';
            $data['action'] = 'add';
            $data['pagess'] = $this->PageModel->GetAllData();
            $data['action_form'] = site_url('pages/PagesActionForm');
            $data['action_delete_pages'] = site_url('pages/Deletepages');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function pagesActionForm() {
//        echo '<pre>';print_r($this->input->post());echo '</pre>';die();
        $this->form_validation->set_rules('code', 'Code', 'required');
        $this->form_validation->set_rules('name', 'pages Name', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');
        $this->form_validation->set_rules('position', 'Position', 'required');
        $this->form_validation->set_rules('status', 'State', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $action = $this->input->post('action');
            $data['code'] = $this->input->post('code');
            $data['url'] = $this->input->post('url');
            $data['name'] = $this->input->post('name');
            $data['position'] = $this->input->post('position');
            $data['status'] = $this->input->post('status');
            $data['image'] = $this->input->post('image');
            if ($action == 'add') {
                $this->pagesModel->Add($data);
            } else {
                $id = $this->input->post('id');
                $this->pagesModel->Update($data, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('pages');
            echo json_encode($data);
            exit();
        }
    }

    public function GetpagesByID() {
        $id = $this->input->post('id');
        if ($id) {
            $pages = $this->pagesModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['code'] = $pages->code;
            $data['name'] = $pages->name;
            $data['url'] = $pages->url;
            $data['image'] = $pages->image;
            $data['position'] = $pages->position;
            $data['status'] = $pages->status;
            $data['error'] = 0;
            $data['id'] = $id;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->pagesModel->delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

    /* Function untuk manage slider */

    function Slider() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'slider/slider';
            $data['action'] = 'add';
            $data['sliders'] = $this->SliderModel->GetAllData();
            $data['action_form'] = site_url('pages/SliderActionForm');
            $data['action_delete_slider'] = site_url('pages/DeleteSlider');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function deleteSlider() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->SliderModel->delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

    public function GetSliderByID() {
        $id = $this->input->post('id');
        if ($id) {
            $slider = $this->SliderModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['description'] = $slider->description;
            $data['title'] = $slider->title;
            $data['position'] = $slider->position;
            $data['image'] = $slider->image;
            $data['created_at'] = $slider->created_at;
            $data['error'] = 0;
            $data['id'] = $id;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function SliderActionForm() {
//        echo '<pre>';print_r($this->input->post());echo '</pre>';die();
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('image', 'image', 'required');
        $this->form_validation->set_rules('position', 'Position', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $action = $this->input->post('action');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['image'] = $this->input->post('image');
            $data['position'] = $this->input->post('position');
            if ($action == 'add') {
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->SliderModel->Add($data);
            } else {
                $id = $this->input->post('id');
                $this->SliderModel->Update($data, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('pages/Slider');
            echo json_encode($data);
            exit();
        }
    }

}