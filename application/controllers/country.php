<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Country extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('MerchantModel','CountryModel'));
    }

    //get zone by country_id
    function GetSelectBoxZone() {
        $CountryID = $this->input->post('country_id');
        $zones = $this->CountryModel->GetZoneByCountry($CountryID);
        if ($zones->num_rows > 0) {
            echo '<option value=""> - Pilih Provinsi - </option>';
            foreach ($zones->result() as $zone){
                echo '<option value="'.$zone->zone_id.'">'.$zone->name.'</option>';
            }
        }
    }

    //get city by country_id
    function GetSelectBoxCity() {
        $ZoneID = $this->input->post('zone_id');
        $cities = $this->CountryModel->GetCityByZone($ZoneID);
        if($cities->num_rows > 0) {
             echo '<option value=""> - Pilih Kota - </option>';
             foreach ($cities->result() as $city){
                echo '<option value="'.$city->city_id.'">'.$city->name.'</option>';
            }
        }
    }

}