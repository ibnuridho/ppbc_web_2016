<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('UserModel', 'MasterAccountBankModel','MasterMaskapaiModel'));
    }

    function AccountBank() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'master/account_bank';
            $data['action'] = 'add';
            $data['banks'] = $this->MasterAccountBankModel->GetAllData();
            $data['action_form'] = site_url('master/BankActionForm');
            $data['action_delete_bank'] = site_url('master/DeleteBank');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }



    function Maskapai() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'master/master_maskapai';
            $data['action'] = 'add';
            $data['maskapais'] = $this->MasterMaskapaiModel->GetAllData();
            $data['action_form'] = site_url('master/MaskapaiActionForm');
            $data['action_delete_maskapai'] = site_url('master/DeleteMaskapai');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function BankActionForm() {
//        echo '<pre>';print_r($this->input->post());echo '</pre>';die();
        $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
        $this->form_validation->set_rules('account_name', 'Account Name', 'required');
        $this->form_validation->set_rules('status', 'State', 'required');
        $this->form_validation->set_rules('no_rekening', 'Rekening Number', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $action = $this->input->post('action');
            $bank['bank_name'] = $this->input->post('bank_name');
            $bank['account_name'] = $this->input->post('account_name');
            $bank['no_rekening'] = $this->input->post('no_rekening');
            $bank['status'] = $this->input->post('status');
            $bank['image'] = $this->input->post('image');
            if ($action == 'add') {
                $this->MasterAccountBankModel->Add($bank);
            } else {
                $id = $this->input->post('id');
                $this->MasterAccountBankModel->Update($bank, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('master/AccountBank');
            echo json_encode($data);
            exit();
        }
    }


    public function MaskapaiActionForm() {
        $this->form_validation->set_rules('name', 'Maskapai Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $action = $this->input->post('action');
            $maskapai['name'] = $this->input->post('name');
            $maskapai['description'] = $this->input->post('description');
            $maskapai['image'] = $this->input->post('image');
            if ($action == 'add') {
                $this->MasterMaskapaiModel->Add($maskapai);
            } else {
                $id = $this->input->post('id');
                $this->MasterMaskapaiModel->Update($maskapai, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('master/Maskapai');
            echo json_encode($data);
            exit();
        }
    }




    public function GetDataMaskapaiByID() {
        $id = $this->input->post('id');
        if ($id) {
            $maskapai = $this->MasterMaskapaiModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['name'] = $maskapai->name;
            $data['description'] = $maskapai->description;
            $data['image'] = $maskapai->image;
            $data['error'] = 0;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function GetDataBankByID() {
        $id = $this->input->post('id');
        if ($id) {
            $bank = $this->MasterAccountBankModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['bank_name'] = $bank->bank_name;
            $data['account_name'] = $bank->account_name;
            $data['no_rekening'] = $bank->no_rekening;
            $data['image'] = $bank->image;
            $data['status'] = $bank->status;
            $data['error'] = 0;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }


    public function DeleteMaskapai() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->MasterMaskapaiModel->Delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }


    public function DeleteAccountBank() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->MasterAccountBankModel->Delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

}
