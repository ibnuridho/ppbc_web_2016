<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Berita extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('UserModel', 'BeritaModel','SliderModel'));
    }

    function Index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'berita/berita';
            $data['action'] = 'add';
            $params = $_GET['params'];

            if ($params) {
             $data['beritas'] = $this->BeritaModel->GetAllDataPage($params);
            }else{ 
             $data['beritas'] = $this->BeritaModel->GetAllData();
            }
            $data['action_form'] = site_url('berita/BeritaActionForm');
            $data['action_delete_berita'] = site_url('berita/DeleteBerita');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function BeritaActionForm() {
//        echo '<pre>';print_r($this->input->post());echo '</pre>';die();
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        // $this->form_validation->set_rules('name', 'Berita Name', 'required');
        // $this->form_validation->set_rules('url', 'URL', 'required');
        // $this->form_validation->set_rules('image', 'Image', 'required');
        // $this->form_validation->set_rules('position', 'Position', 'required');
        $this->form_validation->set_rules('status', 'State', 'required');
        $this->form_validation->set_rules('prolog', 'Prolog', 'required');
        $this->form_validation->set_rules('isi', 'Isi Berita', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $param = $this->input->post('param');
            $action = $this->input->post('action');
            $data['judul'] = $this->input->post('judul');
            // $data['url'] = $this->input->post('url');
            // $data['name'] = $this->input->post('name');
            $data['param'] = $param;
            $data['status'] = $this->input->post('status');
            $data['image'] = $this->input->post('image');
            $data['prolog'] = $this->input->post('prolog');
            $data['isi'] = $this->input->post('isi');
            if ($action == 'add') {
                $this->BeritaModel->Add($data);
            } else {
                $id = $this->input->post('id');
                $this->BeritaModel->Update($data, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('berita/berita?params=' . $param);
            echo json_encode($data);
            exit();
        }
    }

    public function GetBeritaByID() {
        $id = $this->input->post('id');
        if ($id) {
            $berita = $this->BeritaModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['code'] = $berita->code;
            $data['name'] = $berita->name;
            $data['url'] = $berita->url;
            $data['image'] = $berita->image;
            $data['position'] = $berita->position;
            $data['status'] = $berita->status;
            $data['description'] = $berita->description;
            $data['error'] = 0;
            $data['id'] = $id;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->BeritaModel->delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

    /* Function untuk manage slider */

    function Slider() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'slider/slider';
            $data['action'] = 'add';
            $data['sliders'] = $this->SliderModel->GetAllData();
            $data['action_form'] = site_url('berita/SliderActionForm');
            $data['action_delete_slider'] = site_url('berita/DeleteSlider');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function deleteSlider() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->SliderModel->delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

    public function GetSliderByID() {
        $id = $this->input->post('id');
        if ($id) {
            $slider = $this->SliderModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['description'] = $slider->description;
            $data['title'] = $slider->title;
            $data['position'] = $slider->position;
            $data['image'] = $slider->image;
            $data['created_at'] = $slider->created_at;
            $data['error'] = 0;
            $data['id'] = $id;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function SliderActionForm() {
//        echo '<pre>';print_r($this->input->post());echo '</pre>';die();
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('image', 'image', 'required');
        $this->form_validation->set_rules('position', 'Position', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $action = $this->input->post('action');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['image'] = $this->input->post('image');
            $data['position'] = $this->input->post('position');
            if ($action == 'add') {
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->SliderModel->Add($data);
            } else {
                $id = $this->input->post('id');
                $this->SliderModel->Update($data, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('berita/Slider');
            echo json_encode($data);
            exit();
        }
    }

}