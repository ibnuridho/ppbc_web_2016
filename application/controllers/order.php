<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('OrderModel'));
    }

    public function index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'order/list';
            $data['orders'] = $this->OrderModel->getNewOrder();
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }
	
	public function detail($id) {
		if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'order/detail_order';
			$data['detail'] = $this->OrderModel->GetDataByID($id);
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
	}



    public function request_detail($id) {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'order/request_detail';
            $data['detail'] = $this->OrderModel->GetDatarequestByID($id);
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }


    public function request() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'order/request_list';
            $data['requests'] = $this->OrderModel->getAllRequest();
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }


    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->OrderModel->Delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }
}