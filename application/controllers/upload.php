<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('ProductImageModel'));
    }

    public function ImageProduct() {
        $upload_directory = FCPATH . 'assets/images/products/';
        $file = $upload_directory . basename($_FILES['image']['name']);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) {
            if (file_exists($file)) {
                $image['image'] = $_FILES['image']['name'];
                $image['product_id'] = $this->input->post('product_id');
                $id = $this->ProductImageModel->Add($image);
                $data = array(
                    'image_id' => $id,
                    'result' => 'success',
                    'filename' => $_FILES['image']['name']
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'result' => 'failed',
                    'filename' => $_FILES['image']['name']
                );
                echo (json_encode($data));
                exit();
            }
        } else {
            $data = array(
                'result' => 'failed',
                'filename' => $_FILES['image']['name']
            );
            echo (json_encode($data));
            exit();
        }
    }

    public function LogoBank() {
        $upload_directory = FCPATH . 'assets/images/logo_bank/';
        $file = $upload_directory . basename($_FILES['image']['name']);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) {
            if (file_exists($file)) {
                $image['image'] = $_FILES['image']['name'];
                $data = array(
                    'result' => 'success',
                    'filename' => $_FILES['image']['name']
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'result' => 'failed',
                    'filename' => $_FILES['image']['name']
                );
                echo (json_encode($data));
                exit();
            }
        } else {
            $data = array(
                'result' => 'failed',
                'filename' => $_FILES['image']['name']
            );
            echo (json_encode($data));
            exit();
        }
    }

    public function Banner() {
        $upload_directory = FCPATH . 'assets/images/banner/';
        $file = $upload_directory . basename($_FILES['image']['name']);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) {
            if (file_exists($file)) {
                $image['image'] = $_FILES['image']['name'];
                $data = array(
                    'result' => 'success',
                    'filename' => $_FILES['image']['name']
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'result' => 'failed',
                    'filename' => $_FILES['image']['name']
                );
                echo (json_encode($data));
                exit();
            }
        } else {
            $data = array(
                'result' => 'failed',
                'filename' => $_FILES['image']['name']
            );
            echo (json_encode($data));
            exit();
        }
    }


    public function Slider() {
        $upload_directory = FCPATH . 'assets/images/slider/';
        $file = $upload_directory . basename($_FILES['image']['name']);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) {
            if (file_exists($file)) {
                $image['image'] = $_FILES['image']['name'];
                $data = array(
                    'result' => 'success',
                    'filename' => $_FILES['image']['name']
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'result' => 'failed',
                    'filename' => $_FILES['image']['name']
                );
                echo (json_encode($data));
                exit();
            }
        } else {
            $data = array(
                'result' => 'failed',
                'filename' => $_FILES['image']['name']
            );
            echo (json_encode($data));
            exit();
        }
    }




    public function Maskapai() {
        $upload_directory = FCPATH . 'assets/images/maskapai/';
        $file = $upload_directory . basename($_FILES['image']['name']);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) {
            if (file_exists($file)) {
                $image['image'] = $_FILES['image']['name'];
                $data = array(
                    'result' => 'success',
                    'filename' => $_FILES['image']['name']
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'result' => 'failed',
                    'filename' => $_FILES['image']['name']
                );
                echo (json_encode($data));
                exit();
            }
        } else {
            $data = array(
                'result' => 'failed',
                'filename' => $_FILES['image']['name']
            );
            echo (json_encode($data));
            exit();
        }
    }
    public function UploadProfileUser() {
        $upload_directory = FCPATH . 'assets/images/';
        $file = $upload_directory . basename($_FILES['image']['name']);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) {
            if (file_exists($file)) {
                $image['image'] = $_FILES['image']['name'];
                $data = array(
                    'result' => 'success',
                    'filename' => $_FILES['image']['name']
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'result' => 'failed',
                    'filename' => $_FILES['image']['name']
                );
                echo (json_encode($data));
                exit();
            }
        } else {
            $data = array(
                'result' => 'failed',
                'filename' => $_FILES['image']['name']
            );
            echo (json_encode($data));
            exit();
        }
    }

}
