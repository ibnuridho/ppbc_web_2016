<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();
        $this->load->model(array('ProductModel','SliderModel','ProductImageModel','BannerModel','MasterMaskapaiModel','SettingModel','BeritaModel'));
    }

    public function admin() {
        $data['page'] = 'dashboard/dashboard';
        $this->load->view('mainContent', $data);
    }


    public function about() {
        $seo_variable = $this->seotools->seo_tools('about_us', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['page'] = 'frontend/about_us';
        $this->load->view('frontend/mainContent', $data);
    }

    public function contact() {
        $seo_variable = $this->seotools->seo_tools('contact_us', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $this->load->helper(array('captcha'));
        $data['messages'] = '';
        $data['success'] = '';
        $captcha = create_captcha(array(
            'word' => strtoupper(substr(md5(time()), 0, 6)),
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/'
        ));
        $data['captcha'] = $captcha;
        // store the captcha word in a session
        $this->session->set_userdata('captcha', $captcha['word']);
        $data['page'] = 'frontend/contact_us';
        $this->load->view('frontend/mainContent', $data);
    }

    public function index() {
        $seo_variable = $this->seotools->seo_tools('','');
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        //get latest trip
        $data['sliders'] = $this->SliderModel->GetAllData();
        $data['beritas'] = $this->BeritaModel->GetAllData('berita');
        $data['beritadukas'] = $this->BeritaModel->GetAllData('beritaduka');
        $data['beritasehats'] = $this->BeritaModel->GetAllData('beritasehat');
        $data['headline'] = $this->BeritaModel->GetHeadlineData();
        $data['trips'] = $this->ProductModel->GetLatestTrips(2);
        $this->load->view('frontend/homepage', $data);
    }

    
    public function maskapai() {
        $seo_variable = $this->seotools->seo_tools('maskapai', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['maskapais'] = $this->MasterMaskapaiModel->GetAllData();
        $data['setting'] = $this->SettingModel->getBannerMaskapai();
        $data['page'] = 'frontend/maskapai';
        $this->load->view('frontend/mainContent', $data);
    }

    public function termconditions() {
        $seo_variable = $this->seotools->seo_tools('termconditions', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['maskapais'] = $this->MasterMaskapaiModel->GetAllData();
        $data['setting'] = $this->SettingModel->getBannerMaskapai();
        $data['page'] = 'frontend/termconditions';
        $this->load->view('frontend/mainContent', $data);
    }

    public function faq() {
        $seo_variable = $this->seotools->seo_tools('faq', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['page'] = 'frontend/faq';
        $this->load->view('frontend/mainContent', $data);
    }
    public function agenda() {
        $seo_variable = $this->seotools->seo_tools('faq', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['page'] = 'frontend/agenda';
        $this->load->view('frontend/mainContent', $data);
    }

    public function data_anggota() {
        $seo_variable = $this->seotools->seo_tools('data_anggota', $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['page'] = 'frontend/data_anggota';
        $this->load->view('frontend/mainContent', $data);
    }

    public function tentang_kami($submenu) {
        $seo_variable = $this->seotools->seo_tools($submenu, $id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['PageHeaderTittle'] = $submenu;
        $data['page'] = 'frontend/tentang_kami';
        $this->load->view('frontend/mainContent', $data);
    }

    public function book($id) {
        if ($id) {
                $seo_variable = $this->seotools->seo_tools('detail',$id);
                $data['PageTitle'] = $seo_variable['PageTitle'];
                $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
                $data['PageDesc'] = $seo_variable['PageDesc'];
                $data['detail'] = $this->ProductModel->GetProductByID($id);

                $this->load->helper(array('captcha'));

                $captcha = create_captcha(array(
                    'word' => strtoupper(substr(md5(time()), 0, 6)),
                    'img_path' => './captcha/',
                    'img_url' => base_url() . 'captcha/'
                ));
                $data['captcha'] = $captcha;
                // store the captcha word in a session
                $this->session->set_userdata('captcha', $captcha['word']);
                $data['page'] = 'frontend/book';
                $this->load->view('frontend/mainContent', $data);
            } else {
                redirect('index');
            }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */