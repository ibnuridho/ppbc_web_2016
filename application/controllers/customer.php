<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('CustomerModel','OrderModel'));
    }

    function index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['customers'] = $this->CustomerModel->GetAllCustomer();
            $data['page'] = 'customer/list';
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    function SetActive() {
        $id = $this->input->post('id');
    }

    function SetDeactive() {
        $id = $this->input->post('id');
    }

    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->CustomerModel->Delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function sendContact() {
        $this->form_validation->set_rules('name', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('content', 'Content', 'required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['messages'] = validation_errors();
            $data['success'] = '';
            $seo_variable = $this->seotools->seo_tools('contact_us', $id=0);
            $data['PageTitle'] = $seo_variable['PageTitle'];
            $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
            $data['PageDesc'] = $seo_variable['PageDesc'];
            $this->load->helper(array('captcha'));
            $captcha = create_captcha(array(
                'word' => strtoupper(substr(md5(time()), 0, 6)),
                'img_path' => './captcha/',
                'img_url' => base_url() . 'captcha/'
            ));
            $data['captcha'] = $captcha;
            // store the captcha word in a session
            $this->session->set_userdata('captcha', $captcha['word']);
            $data['page'] = 'frontend/contact_us';
            $this->load->view('frontend/mainContent', $data);
        } else if ($this->session->userdata['captcha'] != $this->input->post('captcha')) {
            $data['messages'] = 'Captcha code tidak cocok, mohon coba lagi.';
            $data['success'] = '';
            $data['messages'] = '';
            $seo_variable = $this->seotools->seo_tools('contact_us', $id=0);
            $data['PageTitle'] = $seo_variable['PageTitle'];
            $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
            $data['PageDesc'] = $seo_variable['PageDesc'];
            $this->load->helper(array('captcha'));
            $captcha = create_captcha(array(
                'word' => strtoupper(substr(md5(time()), 0, 6)),
                'img_path' => './captcha/',
                'img_url' => base_url() . 'captcha/'
            ));
            $data['captcha'] = $captcha;
            // store the captcha word in a session
            $this->session->set_userdata('captcha', $captcha['word']);
            $data['page'] = 'frontend/contact_us';
            $this->load->view('frontend/mainContent', $data);
        } else {
            $contact['name'] = $this->input->post('name');
            $contact['email'] = $this->input->post('email');
            $contact['content'] = $this->input->post('content');
            $contact['submited_at'] = date('Y-m-d H:i:s');
            
            $this->CustomerModel->AddContact($contact);
            $this->KirimEmail('safairuz@yahoo.co.id', '[Notification - Manatrip] You Have A New Message', 'email/contact_us', $contact);
            
            $this->KirimEmail($contact['email'], 'Thank You for contacting us.', 'email/thankyou', $contact);

            $data['messages'] = '';
            $data['success'] = 'Terimakasih, pesan anda telah terkirim.';
            $seo_variable = $this->seotools->seo_tools('contact_us', $id=0);
            $data['PageTitle'] = $seo_variable['PageTitle'];
            $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
            $data['PageDesc'] = $seo_variable['PageDesc'];
            $this->load->helper(array('captcha'));
            $captcha = create_captcha(array(
                'word' => strtoupper(substr(md5(time()), 0, 6)),
                'img_path' => './captcha/',
                'img_url' => base_url() . 'captcha/'
            ));
            $data['captcha'] = $captcha;
            // store the captcha word in a session
            $this->session->set_userdata('captcha', $captcha['word']);
            $data['page'] = 'frontend/contact_us';
            $this->load->view('frontend/mainContent', $data);
        }
    }

	public function receiveBook() {
        $this->form_validation->set_rules('name', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('member_total', 'Jumlah Orang', 'numeric');
        $this->form_validation->set_rules('phone', 'Phone', 'numeric');
		if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
        	$book['name'] = $this->input->post('name');
        	$book['email'] = $this->input->post('email');
        	$book['phone'] = $this->input->post('phone');
        	$book['trip_name'] = $this->input->post('trip_name');
        	$book['product_id'] = $this->input->post('product_id');
        	$book['address'] = $this->input->post('address');
        	$book['date'] = date('Y-m-d',strtotime($this->input->post('date')));
        	$book['member_total'] = $this->input->post('member_total');
        	$book['content'] = $this->input->post('content');
            $book['submited_at'] = date('Y-m-d H:i:s');
            $book['is_read'] = 0;

            $this->OrderModel->Add($book);
            
            $this->KirimEmail('safairuz@yahoo.co.id', '[Notification - Manatrip] You Have A New Order', 'email/new_order', $book);
            $this->KirimEmail($book['email'], 'Thank You for contacting us.', 'email/thankyou_order', $book);

            $data['error'] = 0;
            $data['message'] = 'Pemesanan '.$book['trip_name']. ' telah terkirim, kami akan menghubungi anda secepatnya.';
            echo json_encode($data);
            exit();
        }
	}

    public function KirimEmail($to, $subject, $template, $options) {
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'cemilerteam@gmail.com';
        $config['smtp_pass'] = 'Kumaha Maneh We';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['newline'] = "\r\n";
        $this->email->initialize($config);

        $mailbody = $this->load->view($template, $options, true);

        $this->email->from('cemilerteam@gmail.com', 'Support Manatrip');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($mailbody);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
//            echo $this->email->print_debugger();
        }
    }
/*    public function KirimEmail($to, $subject, $template, $options) {
        try {
            $mailbody = $this->load->view($template, $options, true);
            $this->phpmailer->IsSMTP();
            $this->phpmailer->From = "admin@PPBC.org";
            $this->phpmailer->FromName = "PPBC.org";
            $this->phpmailer->AddAddress($to);
            $this->phpmailer->Subject = $subject;
            $this->phpmailer->AltBody = "To view the message, please use an HTML compatible email viewer!";
            $this->phpmailer->WordWrap = 80;
            $this->phpmailer->MsgHTML(preg_replace('/\\\\/', '', $mailbody));
            $this->phpmailer->IsHTML(true);
            $this->phpmailer->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        }
    }*/
}