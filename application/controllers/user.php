<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('UserModel'));
    }

    public function Index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'user/list';
            $data['users'] = $this->UserModel->GetAllUser();
            $data['action_edit_user'] = site_url('user/edit');
            $data['action_delete_user'] = site_url('user/delete');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function add() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'user/user_form';
            $data['action'] = 'add';
            $data['action_form'] = site_url('user/ActionUser');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function edit($id) {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'user/user_form';
            $data['action'] = 'edit';
            $data['user'] = $this->UserModel->GetUserByID($id)->row();
            $data['action_form'] = site_url('user/ActionUser');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->UserModel->Delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    //$data['redirect'] = site_url('user/ListUser');
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function ActionUser() {
//        echo '<pre>';print_r($this->input->post());echo '</pre>';die();
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $ExistingUsername = $this->UserModel->CheckUserName($username);
            $ExistingEmail = $this->UserModel->CheckEmail($email);
            $action = $this->input->post('action');
            if ($action == 'add') {
                if ($ExistingUsername) {
                    $data['error'] = 1;
                    $data['message'] = 'Username ' . $username . ' already exists, please use another username.';
                    $data['title'] = "Alert";
                    echo json_encode($data);
                    exit();
                } else if ($ExistingEmail) {
                    $data['error'] = 1;
                    $data['message'] = 'Email ' . $email . ' already exists, please use another email.';
                    $data['title'] = "Alert";
                    echo json_encode($data);
                    exit();
                }
            }
            $data['firstname'] = $this->input->post('firstname');
            $data['lastname'] = $this->input->post('lastname');
            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['image'] = $this->input->post('image');
            $data['status'] = $this->input->post('status');
            $data['password'] = md5($this->input->post('password'));

            if ($action == 'add') {
                $this->UserModel->Add($data);
            } else {
                $id = $this->input->post('id');
                $this->UserModel->Update($data, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('user');
            echo json_encode($data);
            exit();
        }
    }

    public function logout() {
        $UserData = $this->session->all_userdata();
        foreach ($UserData as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
        $this->session->sess_destroy();
        redirect('login');
    }

}