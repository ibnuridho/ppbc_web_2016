<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('UserModel'));
    }

    public function Index() {
        $this->load->view('login');
    }

    public function LoginProcess() {
        //custome validation
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('username', 'Username ', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $browser = $this->agent->browser;
            $ip = $this->GetClientIP();
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));

            //$text = "Try to login Username=" . $username . " pada jam " . date("d-M-Y H:i") . " dengan IP " . $ip . ", hit ke server 10.2.0.111\n";
            //file_put_contents("log.txt", $text, FILE_APPEND);

            if (strpos($username, '@')) {
                $log = $this->UserModel->CheckUserByEmail($username, $password);
            } else {
                $log = $this->UserModel->CheckUserByUsername($username, $password);
            }


            if ($log->num_rows > 0) {
                $user = $log->row();
                if($user->status == 0) {                
                    $data['error'] = 1;
                    $data['message'] = 'User is Deactive.';
                    $data['title'] = "Alert";
                    echo json_encode($data);
                    exit();
                }
                $id = $user->id;
                $firstname = $user->firstname;
                $lastname = $user->lastname;
                $username = $user->username;
                $email = $user->email;
                $position = $user->position;
                $password = $user->password;
                $created_at = $user->created_at;
                if ($user->image) {
                    // $photo = $user->image;
                } else {
                    // $photo = 'no_image.png';
                }

                //$text = "Username=" . $username . ", berhasil login @" . date("d-M-Y H:i") . " dengan IP " . $ip . ", hit ke server 10.2.0.111\n";
                //file_put_contents("log.txt", $text, FILE_APPEND);

                //initial session
                $this->session->set_userdata(array(
                    'UserIDSession' => $id,
                    'UserNameSession' => $username,
                    'FirstNameSession' => $firstname,
                    'LastNameSession' => $lastname,
                    'PositionSession' => $position,
                    'EmailSession' => $email,
                    'CreatedAtSession' => $created_at,
                    // 'PhotoSession' => $photo,
                    'is_loged_in' => true
                ));

                if ($this->input->post('remember_me')) {
                    $this->config->set_item('sess_expire_on_close', '0');
                    $this->load->library('session');
                }
                //$text = "Username=" . $username . ", Session username=" . $this->session->userdata('UserNameSession') . "\n";
                //file_put_contents("log.txt", $text, FILE_APPEND);


                $data['error'] = 0;
                $data['message'] = 'Login Success';
                $data['redirect'] = site_url('dashboard');
                echo json_encode($data);
                exit();
            } else {
                $data['error'] = 1;
                $data['message'] = 'Incorrect User ID or Password';
                $data['title'] = "Alert";
                echo json_encode($data);
                exit();
            }
        }
    }

    function GetClientIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}