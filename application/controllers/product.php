<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model(array('ProductModel','ProductImageModel','BannerModel', 'SliderModel','OrderModel'));
    }

    function Index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['products'] = $this->ProductModel->GetAllProduct();
            $data['page'] = 'product/list';
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    function add() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'product/product_form';
            $data['action'] = 'add';
            $data['action_form'] = site_url('product/ActionProduct');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function edit($id) {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'product/product_form';
            $data['action'] = 'edit';
            $data['product'] = $this->ProductModel->GetProductByID($id);
            $data['action_form'] = site_url('product/ActionProduct');
            $data['images'] = $this->ProductModel->GetImagesProductByID($id);
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    function ActionProduct() {
        $this->form_validation->set_rules('name', 'Product Name', 'required');
        // $this->form_validation->set_rules('price', 'Additional Price', 'required');
        $this->form_validation->set_rules('duration', 'Duration Trip', 'required|numeric');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $product_name = $this->input->post('name');
            $description = $this->input->post('description');
            $highlight = $this->input->post('highlight');
            $meta_description = $this->input->post('meta_description');
            $meta_keyword = $this->input->post('meta_keyword');
            $price = 0;
            $duration = $this->input->post('duration');
//            $status = $this->input->post('status');
            $action = $this->input->post('action');
            $slug = $this->slug->GenerateSlug($product_name);
            $CheckProductName = $this->ProductModel->CheckExistingProductBySlug($slug);
            if ($CheckProductName && $action == 'add') {
                $data['error'] = 1;
                $data['message'] = 'Trip ' . $product_name . ' already exists, please use another product.';
                $data['title'] = "Alert";
                echo json_encode($data);
                exit();
            } else {
                $products['name'] = $product_name;
                $products['description'] = $description;
                $products['highlight'] = $highlight;
                $products['meta_description'] = $meta_description;
                $products['meta_keyword'] = $meta_keyword;
                $products['price'] = $price;
                $products['duration'] = $duration;
                $products['slug'] = $slug;

                $id_saved = 0;
                $updated = 0;
                if ($action == 'add') {
                    $products['created_at'] = date('Y-m-d H:i:s');
                    $id_saved = $this->ProductModel->Add($products);
                    $data['id'] = $id_saved;
                } else {
                    $id = $this->input->post('id');
                    $data['id'] = $id;
                    $updated = $this->ProductModel->Update($products, $id);
                }

                if ($id_saved || $updated) {
                    $data['error'] = 0;
                    $data['message'] = 'Data Sukses Tersimpan.';
                    $data['title'] = "Alert";
                    $data['redirect'] = site_url('product');
                    echo json_encode($data);
                    exit();
                }
            }
        }
    }


    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->ProductModel->Delete($id);
                //delete image by product id
                $this->ProductImageModel->DeleteByProductID($id);
                if ($deleted) {
                    $data['error'] = 0;
                    //$data['redirect'] = site_url('user/ListUser');
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

    public function DeleteImage() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->ProductImageModel->Delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }


    public function SetPrimeImage() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            $product_id = $this->input->post('product_id');
            if ($id && $product_id) {
                $image = $this->ProductImageModel->GetImageNameByID($id);
                //update image on table product
                $field['prime_image'] = $image;
                $this->ProductModel->Update($field, $product_id);
                $data['message'] = 'Image Berhasil di Pilih!';
                $data['error'] = 0;
                echo json_encode($data);
                exit();
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }

    //get image description untuk setiap image
    function GetDescriptionImage() {
        $id = $this->input->post('id');
        $image = $this->ProductImageModel->GetImageByID($id);
        $data['error'] = 0;
        $data['description'] = $image->description;
        echo json_encode($data);
        exit();
    }
    
    function AddImageDescription(){
        $id = $this->input->post('image_id');
        $description = $this->input->post('description_image');
        $images['description'] = $description;
        $updated = $this->ProductImageModel->Update($images, $id);
        $data['error'] = 0;
        $data['message'] = 'Success update description';
        echo json_encode($data);
        exit();
        
    }

    /*FRONT END FUNCTION*/
    public function trip () {
        $seo_variable = $this->seotools->seo_tools('package_mode_detail',$id=0);
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['page'] = 'frontend/trip_list';
        $data['trips'] = $this->ProductModel->GetAllProduct();
        $this->load->view('frontend/mainContent', $data);
    }

    public function detail($slug) {
        if ($slug) {
            $id = $this->ProductModel->GetTripIDBySlug($slug);
            if ($id) {
                $seo_variable = $this->seotools->seo_tools('detail',$id);
                $data['PageTitle'] = $seo_variable['PageTitle'];
                $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
                $data['PageDesc'] = $seo_variable['PageDesc'];
                $data['images'] = $this->ProductModel->GetImagesTripByID($id);
                $data['detail'] = $this->ProductModel->GetProductByID($id);

                $data['banners'] = $this->BannerModel->GetLatestBanner(2);
                
                $data['page'] = 'frontend/trip_detail';
                $this->load->view('frontend/mainContent', $data);
            } else {
                redirect('trip');
            }
        } else {
            redirect('trip');
        }
    }

    public function custome_request() {
        $seo_variable = $this->seotools->seo_tools('custome_request', $id=0);
        $data['sliders'] = $this->SliderModel->GetAllData();
        $data['PageTitle'] = $seo_variable['PageTitle'];
        $data['PageKeyWords'] = $seo_variable['PageKeyWords'];
        $data['PageDesc'] = $seo_variable['PageDesc'];
        $data['messages'] = '';
        $data['success'] = '';
        $data['page'] = 'frontend/trip_request';
        $this->load->view('frontend/mainContent', $data);
    }


    public function CustomeRequestProccess() {
        // $this->form_validation->set_rules('type', 'Type Trip', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('budget', 'Budget', 'required');
        $this->form_validation->set_rules('duration', 'Lama Liburan', 'required');
        $this->form_validation->set_rules('destination', 'Destination', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'numeric');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else if ($this->input->post('nature') == '' && $this->input->post('urban') == '') {
            $data['error'] = 1;
            $data['message'] = 'Anda harus memilih, minimal satu type Trip.';
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $book['name'] = $this->input->post('name');
            $book['email'] = $this->input->post('email');
            $book['budget'] = $this->input->post('budget');
            $book['phone'] = $this->input->post('phone');
            $book['type'] = $this->input->post('nature').' - '.$this->input->post('urban');
            $book['duration'] = $this->input->post('duration');
            $book['destination'] = $this->input->post('destination');
            $book['friend'] = $this->input->post('friend');
            $book['comment'] = $this->input->post('comment');
            $book['address'] = $this->input->post('address');
            $book['member'] = $this->input->post('member');
            $book['date'] = date('Y-m-d',strtotime($this->input->post('date')));
            $book['created_at'] = date('Y-m-d H:i:s');


            $this->OrderModel->AddRequestTrip($book);
            $this->KirimEmail('safairuz@yahoo.co.id', '[Notification - Manatrip] You Have A New Order', 'email/new_request', $book);
            $this->KirimEmail($book['email'], 'Thank You for Sending Request.', 'email/thankyou_request', $book);
            
            $data['error'] = 0;
            $data['message'] = 'Request terkirim, kami akan segera menghubungi anda.';
            echo json_encode($data);
            exit();
        }
    }

    
    public function KirimEmail($to, $subject, $template, $options) {
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'cemilerteam@gmail.com';
        $config['smtp_pass'] = 'Kumaha Maneh We';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['newline'] = "\r\n";
        $this->email->initialize($config);

        $mailbody = $this->load->view($template, $options, true);

        $this->email->from('cemilerteam@gmail.com', 'Support Manatrip');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($mailbody);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
//            echo $this->email->print_debugger();
        }
    }
}