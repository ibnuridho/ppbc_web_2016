<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('SettingModel'));
    }

    function Index() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'setting/setting_banner_maskapai';
            // $data['action'] = 'edit';
            $data['settings'] = $this->SettingModel->GetAllData();
            $data['action_form'] = site_url('banner/BannerActionForm');
            // $data['action_delete_banner'] = site_url('setting/DeleteBanner');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    function BannerMaskapai() {
        if ($this->session->userdata('UserIDSession')) {
            $data['page'] = 'setting/setting_banner_maskapai';
            $data['action'] = 'add';
            $data['settings'] = $this->SettingModel->GetAllData();
            $data['action_form'] = site_url('setting/SettingBannerMaskapaiActionForm');
            // $data['action_delete_banner'] = site_url('setting/DeleteBanner');
            $this->load->view('mainContent', $data);
        } else {
            redirect('login');
        }
    }

    public function SettingBannerMaskapaiActionForm() {
        $this->form_validation->set_rules('image', 'Image', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $action = $this->input->post('action');
            // $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            // $data['status'] = $this->input->post('status');
            $data['image'] = $this->input->post('image');
            if ($action == 'add') {
                $this->SettingModel->Add($data);
            } else {
                $id = $this->input->post('id');
                $this->SettingModel->Update($data, $id);
            }
            $data['error'] = 0;
            $data['redirect'] = site_url('setting/BannerMaskapai');
            echo json_encode($data);
            exit();
        }
    }

    public function GetBannerMaskpaiByID() {
        $id = $this->input->post('id');
        if ($id) {
            $banner = $this->SettingModel->GetDataByID($id);
            $data['action'] = 'edit';
            $data['code'] = $banner->code;
            $data['image'] = $banner->image;
            $data['description'] = $banner->description;
            // $data['status'] = $banner->status;
            $data['error'] = 0;
            $data['id'] = $id;
            echo json_encode($data);
            exit();
        } else {
            $data['error'] = 1;
            echo json_encode($data);
            exit();
        }
    }

    public function delete() {
        if ($this->session->userdata('UserIDSession')) {
            $id = $this->input->post('id');
            if ($id) {
                $deleted = $this->SettingModel->delete($id);
                if ($deleted) {
                    $data['error'] = 0;
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data['error'] = 1;
                echo json_encode($data);
                exit();
            }
        } else {
            redirect('login');
        }
    }
}