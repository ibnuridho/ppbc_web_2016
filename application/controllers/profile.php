<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('UserModel'));
    }

    public function Index() {
        if ($this->session->userdata('UserIDSession')) {
            $user = $this->UserModel->GetUserByID($this->session->userdata('UserIDSession'));
            if ($user->num_rows > 0) {
                $data['profile'] = $this->UserModel->GetUserDetail($this->session->userdata('UserIDSession'));
                $data['page'] = 'profile/profile';
                $data['action_form_credential'] = site_url('profile/UpdateCredential');
                $data['action_form_info'] = site_url('profile/UpdateInfoUser');
                $this->load->view('mainContent', $data);
            } else {
                redirect('login');
            }
        } else {
            redirect('login');
        }
    }

    //update credential here : password, merchant_code, etc
    public function UpdateInfo() {
        $this->form_validation->set_rules('no_rekening', 'Nomor Rekening', 'required');
        $this->form_validation->set_rules('bank', 'Bank', 'required');
        $this->form_validation->set_rules('merchant_code', 'Term and Conditions', 'required|max_length[4]');
        //checking if password including password to update
        if ($this->input->post('update_password')) {
            $this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim');
        }
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            if ($this->input->post('update_password')) {
                $data['password'] = md5($this->input->post('password'));
            }
            $merchant_id = $this->session->userdata('merchant_id_session');
            $data['merchant_code'] = $this->input->post('merchant_code');
            $data['no_rekening'] = $this->input->post('no_rekening');
            $data['bank'] = $this->input->post('bank');
            $updated = $this->UserModel->Update($data, $merchant_id);
            if ($updated) {
                $data['error'] = 0;
                $data['redirect'] = site_url('profile');
                echo json_encode($data);
                exit();
            }
        }
    }

    //update credential here : password, merchant_code, etc
    public function UpdateCredential() {
        $this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $data['password'] = md5($this->input->post('password'));
            $user_id = $this->session->userdata('UserIDSession');
            $updated = $this->UserModel->Update($data, $user_id);
            if ($updated) {
                $data['error'] = 0;
                $data['redirect'] = site_url('profile');
                echo json_encode($data);
                exit();
            }
        }
    }

    public function UpdateInfoUser() {
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'required');


        if ($this->form_validation->run() == FALSE) {
            $data['error'] = 1;
            $data['message'] = validation_errors();
            $data['title'] = "Alert";
            echo json_encode($data);
            exit();
        } else {
            $user_id = $this->session->userdata('UserIDSession');
            $data['firstname'] = $this->input->post('firstname');
            $data['lastname'] = $this->input->post('lastname');
            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            // $data['photo'] = $this->input->post('photo');
            $updated = $this->UserModel->Update($data, $user_id);
            if ($updated) {
                $data['error'] = 0;
                $data['redirect'] = site_url('profile');
                echo json_encode($data);
                exit();
            }
        }
    }

}